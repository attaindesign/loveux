# [Deploy](https://deploy.co.uk)

Deploy Website created on WordPress using Advanced Custom Fields Pro.
The main CSS files are minified; To modify the source you will need to use Gulp.
The site is deployed using Capistrano. See Tools for Readme on Capistrano.

## Prerequisities

* [Gulp](http://gulpjs.com)
* [Node.js](http://nodejs.org)
* [Capistrano](https://capistranorb.com)

## Repository Structure
* library - contains assets such as css, images, js and scss.
* partials - contains snippets that can be included via get_template_part(); in other pages.
* templates - contains the layouts that are shared between pages.

## Deployment
* To deploy onto an environment we use Capistrano. See tools readme.