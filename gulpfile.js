// 
//       ____             __
//    _ / __ \___  ____  / /___  __  __ ®
//   (_) / / / _ \/ __ \/ / __ \/ / / /
//  _ / /_/ /  __/ /_/ / / /_/ / /_/ /
// (_)_____/\___/ .___/_/\____/\__, /
//             /_/            /____/
// 
//     Thanks for your interest in the source code.
//     Please feel free to have a look around to see how things work, but please don't steal.
//     If you like what we do then please get in touch!
// 
//     www.deploy.co.uk
// 
//     Copyright (c) 2018  :Deploy®. All Rights Reserved.
//
// Modules
// ======================================================
var gulp            = require('gulp'),
    autoprefixer    = require('gulp-autoprefixer'),
    iconfont        = require('gulp-iconfont'),
    iconfontCSS     = require('gulp-iconfont-css'),
    plumber         = require('gulp-plumber'),
    rename          = require('gulp-rename'),
    sass            = require('gulp-sass'),
    size            = require('gulp-size'),
    concat          = require('gulp-concat'),
    $               = require('gulp-load-plugins')(),
    uglify          = require('gulp-uglify'),
    browserSync     = require('browser-sync'),
    fontName        = 'iconFont',
    yargs           = require('yargs').argv,
    name            = yargs.name;
    cleanCSS        = require('gulp-clean-css');

// Icons
// ======================================================
function icons(done) {
    gulp.src(['./wp-content/themes/deploy/library/images/icons/*.svg'])
        .pipe(iconfontCSS({
            fontName: fontName,
            path: './wp-content/themes/deploy/library/scss/fonts/_icon-mixin.scss',
            targetPath: '../scss/fonts/_icons.scss',
            fontPath: '../fonts/'
        }))
        .pipe(iconfont({
            fontName: fontName,
            // Remove woff2 if you get an ext error on compile
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
            normalize: true,
            fontHeight: 1001
        }))
        .pipe(gulp.dest('./wp-content/themes/deploy/library/fonts'))
        .pipe(browserSync.reload({
            stream: true
        }));
    done();
}

// CSS
// ======================================================

// Dev
function css(done) {
    gulp.src('./wp-content/themes/deploy/library/scss/**/*.scss')
        .pipe($.sourcemaps.init())
        .pipe($.plumber())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 8 version']
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('./wp-content/themes/deploy/library/css'))
        .pipe(browserSync.stream());

    done();
}

// Live
function cssLive(done) {
    gulp.src('./wp-content/themes/deploy/library/scss/**/*.scss')
        .pipe($.plumber())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 8 version']
        }))
        .pipe($.cleanCss())
        .pipe($.size({
            showFiles: true
        }))
        .pipe(gulp.dest('./wp-content/themes/deploy/library/css'));

    done();
}

// JS
// ======================================================
function js(done) {
    gulp.src('./wp-content/themes/deploy/library/js-src/**/*.js')
        .pipe(uglify())
        .on('error', onError)
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(concat('all.min.js'))
        .pipe(gulp.dest('./wp-content/themes/deploy/library/js'));

    done();
}

function onError(err) {
    console.log(err);
    this.emit('end');
}

// Browser Sync
// ======================================================

function sync(done) {
    if (name) {
        // BrowserSync location
        browserSync.init({
            notify: true,
            proxy: name
        });
    } else {
        console.log('Browser Sync not set.'),
        console.log('To load the site in browser-sync use --name=*sitename* on end of $ gulp dev --name=*sitename*');
    }
    done();
}

// Watch
// ======================================================

function watch() {
    gulp.watch('./wp-content/themes/deploy/library/scss/**/*.scss', css);
    gulp.watch('./wp-content/themes/deploy/library/images/icons/*', icons);
    gulp.watch('./wp-content/themes/deploy/library/js-src/**/*.js', js);
    
    gulp.watch('./wp-content/themes/deploy/**/*.php').on('change', browserSync.reload);

    // add browserSync.reload to the tasks array to make all browsers reload after tasks are complete.
    gulp.watch('*/deploy/**/*.php').on('change', browserSync.reload);
}


// Help
// ======================================================

function info(done) {
    console.log('Project ready for launch, use "$ gulp live"'),
    console.log('Project in Development, use "$ gulp dev"');
    done();
}

var dev = gulp.series(
    gulp.parallel(
        watch,
        sync
    )
);

var live = gulp.series(
    icons,
    cssLive,
    js
);

exports.default = info;
exports.dev = dev;
exports.live = live;
