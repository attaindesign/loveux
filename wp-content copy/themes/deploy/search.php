<?php
/**
* The template for displaying search results pages
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

<section class="page search-results">
	<?php if (!have_posts()) : ?>
	<div class="search-results__alert search-results__alert-warning">
		<h1 class="page-title page-title__search-results">Sorry, No results found!</h1>
	</div>
    <div class="search-results__form">
        <?php get_search_form(); ?>        
    </div>
	<?php endif; ?>
	<?php get_template_part( 'partials/loop', 'default' ); ?>
	<?php get_template_part( 'partials/content', 'pagination' ); ?>
</section>

<?php get_footer(); ?>
