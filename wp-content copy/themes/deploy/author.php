<?php
/**
* The template for displaying author posts
*
* @package WordPress
* @subpackage Deploy
* 
*/

get_header(); ?>

	<section class="page blog">
        <article class="blog-author">                 
    		<h1 class="page-title page-title__author">Author: <?php echo get_the_author(); ?></h1>
    		<?php
    		if ( get_the_author_meta( 'description' ) ) : 
    		?>
    		<div class="blog-author__info">
    			<div class="blog-author__avatar">
    				<?php echo get_avatar( get_the_author_meta( 'user_email' )); ?>
    			</div>
    			<div class="blog-author__description">
    				<h2><?php printf( __( 'About %s', '' ), get_the_author() ); ?></h2>
    				<?php the_author_meta( 'description' ); ?>
    			</div>
    		</div>
    		<?php endif; ?>
            <section class="blog-author__posts">
                <?php if(have_posts()){ ?>
                    <?php while(have_posts()){ the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="blog-author__post">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php if ( has_post_thumbnail() ) { ?>
                                    <?php the_post_thumbnail('blog-post-thumb'); ?>
                                <?php } ?>
                                <span class="blog-author__title"><?php the_title(); ?></span>
                                <span class="blog-author__date"><?php echo get_the_date( 'l jS \of F Y' ); ?></span>
                            </a>
                        </article>
                    <?php } ?>
                <?php } ?>
                <?php get_template_part( 'partials/content', 'pagination' ); ?>
            </section>
        </article>
	</section>

<?php get_footer(); ?>