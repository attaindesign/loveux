<?php
/**
* The template for displaying all single posts and attachments
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

	<section class="page blog">
		<?php get_template_part( 'partials/content', 'single' ); ?>
	</section>

<div class="page__wrapper">
	<?php get_footer(); ?>
</div>
