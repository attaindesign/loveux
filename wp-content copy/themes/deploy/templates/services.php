<?php
/**
* Template Name: Services Single Page Template
* Description: Main template for the homepage of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page services">
    <div class="services__single">
        <?php
            $intro = get_field('service_intro_text');
        ?>
        
        <section class="services__single--intro content-container padding">
            <div class="content-wrapper">
                <h1 class="title services__single--title" data-aos="fade-up"><?php the_title(); ?></h1>
                <?= ( $intro )? '<div class="services__single-content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
            </div>
        </section>
        
        <?php
            $image = get_field('service_fetured_image');
        ?>
        
        <?php if( $image ): ?>
            <section class="services__single--featured-image section featured-image">
                <div class="bg grey-bg featured-image-bg" data-aos="slide-right" data-aos-duration="1000"></div>
                <div class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" style="background-image: url('<?php echo $image['url']; ?>');"></div>

            </section>
        <?php endif; ?>

        <?php
            // one
            $heading_one = get_field('service_main_content_heading_one');
            $text_one = get_field('service_main_content_text_one');
            $image_one = get_field('service_main_content_image_one');
            // two
            $heading_two = get_field('service_main_content_heading_two');
            $text_two = get_field('service_main_content_text_two');
            $image_two_desktop = get_field('service_main_content_image_two_desktop');
            $image_two_mobile = get_field('service_main_content_image_two_mobile');
        ?>
    
        <section class="services__single--main one section addHeight">
            <div class="content-container padding">
                <div class="content-wrapper">
                    <?= ( $heading_one )? '<h2 class="title" data-aos="fade-up">' . $heading_one .'</h2>' : '';?>
                    <?= ( $text_one )? '<p class="content" data-aos="fade-up" data-aos-delay="100">' . $text_one .'</p>' : '';?>
                </div>
            </div>
            <div class="services-image" data-aos="fade-up" data-aos-delay="300">
                <img src="<?php echo $image_one['url']; ?>" />
            </div>
        </section>
        
        <section class="services__single--main two section">
            <div class="content-container padding">
                <div class="content-wrapper">
                    <?= ( $heading_two )? '<h2 class="title" data-aos="fade-up">' . $heading_two .'</h2>' : '';?>
                    <?= ( $text_two )? '<p class="content" data-aos="fade-up" data-aos-delay="100">' . $text_two .'</p>' : '';?>
                </div>
            </div>
            
            <?php if ( $image_two_desktop ): ?>
                <div class="services-image desktop" data-aos="fade-up">
                    <img src="<?php echo $image_two_desktop['url']; ?>">
                </div>
            <?php endif; ?>
            
            <?php if ( $image_two_mobile ): ?>
                <div class="services-image mobile" data-aos="fade-up">
                    <img src="<?php echo $image_two_mobile['url']; ?>"/>
                </div>
            <?php endif; ?>
        </section>
        
        <?php
            $heading_quote = get_field('service_quote_heading');
            $text_quote = get_field('service_quote_text');
            $author_quote = get_field('service_quote_author');
        ?>
        <section class="services__single--quote quote section">
            
            <div class="quote-content-container padding">
                <div class="content">
                    <div class="quote-bracket left" data-aos="fade-in">
                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/quote-bracket-left.svg" />
                    </div>
                    
                    <div class="content-wrapper">
                        <?= ( $text_quote )? '<p class="text" data-aos="fade-up" data-aos-delay="100">' . $text_quote .'</p>' : '';?>
                        <?= ( $author_quote )? '<p class="author" data-aos="fade-up" data-aos-delay="150">' . $author_quote .'</p>' : '';?>
                    </div>
                    
                    <div class="quote-bracket right" data-aos="fade-in">
                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/quote-bracket-right.svg" />
                    </div>
                </div>
            </div>
        </section>
        
        <section class="services__single--contact contact-partial section">
            <?php get_template_part( 'partials/partial', 'contact' ); ?>
        </section>
        
        <section class="services__single--services services-partial section">
            <?php get_template_part( 'partials/partial', 'services' ); ?>
        </section>

    </div>
</div>

<?php get_footer(); ?>