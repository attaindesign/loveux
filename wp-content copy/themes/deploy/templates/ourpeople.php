<?php
/**
* Template Name: Our People Page Template
* Description: Main template for the our people page of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page people">
    
	<div class="bg grey-bg" data-aos="slide-down" data-aos-delay="200" data-aos-duration="1000"></div>
    <section class="people__intro section addheight">
    <div class="people__intro--intro content-container padding">
        <div class="content-wrapper">
            <?php
                $title = get_field('ourpeople_intro_title');
                $intro = get_field('ourpeople_intro_intro');
                $link = get_field('ourpeople_intro_link');
            ?>
            <?= ( $title )? '<h1 class="title ourpeople__intro--title" data-aos="fade-up">' . $title .'</h1>' : '';?>
            <?= ( $intro )? '<div class="ourpeople__intro--content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
            <?= ( $link )? '<a class="more-link" data-aos="fade-up" data-aos-delay="200" href="' . $link['url'] .'"><span>' . $link['title'] . '</span></a>' : '';?>
        </div>
    </div>   
    </section>
    
    <section class="people__team section">
        <?php if( have_rows('ourepeople_team_members') ): ?>
            <ul class="grid four-column">
    	        <?php while( have_rows('ourepeople_team_members') ): the_row(); 
            		$image = get_sub_field('image');
            		$name = get_sub_field('name');
            		$jobtitle = get_sub_field('job_title');
            		$tel = get_sub_field('telephone');
            		$email = get_sub_field('email');
    		    ?>
                    <li class="grid-item" data-aos="grid-item">
                        <div class="thumbnail-wrapper">
                            <div class="thumbnail fade" style="background-image: url('<?php echo get_sub_field('image')['url']; ?>')"></div>
                        </div>
                        
                        <div class="meta slide">
                            <?= ( $name )? '<h3 class="post-title name">' . $name .'</h3>' : '';?>
                            <p class="input">						
                                <?= ( $jobtitle )? '<p class="cat jobtitle">' . $jobtitle .'</p>' : '';?>
                                <?= ( $tel )? '<a class="cat tel" href="' . $tel['url'] .'">' . $tel['title'] . '</a>' : '';?>
                                <?= ( $email )? '<a class="cat email" href="' . $email['url'] .'">' . $email['title'] . '</a>' : '';?>
                            </p>
                        </div>                        
                    </li>
    	        <?php endwhile; ?>
    	    </ul>
        <?php endif; ?>
    </section>

    <section class="people__join section addheight">
        <div class="people__join--intro content-container padding">
            <div class="content-wrapper">
                <?php
                    $title_join = get_field('ourpeople_join_title');
                    $content_join = get_field('ourpeople_join_content');
                    $link_join = get_field('ourpeople_join_link');
                ?>
                <?= ( $title_join )? '<h1 class="title ourpeople__intro--title" data-aos="fade-up">' . $title_join .'</h1>' : '';?>
                <?= ( $content_join )? '<div class="ourpeople__intro--content content" data-aos="fade-up" data-aos-delay="100">' . $content_join .'</div>' : '';?>
                <?= ( $link_join )? '<a class="more-link" data-aos="fade-up" data-aos-delay="200" href="' . $link_join['url'] .'"><span>' . $link_join['title'] . '</span></a>' : '';?>
            </div>
        </div>   
    </section>
    
    <section class="people__stories section stories">
        <?php get_template_part( 'partials/partial', 'blog' ); ?>
    </section>
</div>

<?php get_footer(); ?>