<?php
/**
* Template Name: Contact Page Template
* Description: Main template for the contact page of theme
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

<div class="page contact">
	<?php
		$intro = get_field('contact_intro_content');
	?>
	<?php if( $intro ): ?>
		<section class="contact__intro section">
	        <div class="contact__intro--intro content-container padding">
	            <div class="content-wrapper">
	                <h1 class="title page__title--single title" data-aos="fade-up"><?php the_title(); ?></h1>
	                <?= ( $intro )? '<div class="contacts__intro--content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
	            </div>
	        </div>
		</section>
	<?php endif; ?>

	<?php // main featured image
		$image = get_field('contact_featured_image');
	?>
	<?php if( $image ): ?>
		<section class="contact__featured-image section featured-image">
			<div class="bg darkgrey-bg featured-image-bg" data-aos="slide-right" data-aos-delay="200" data-aos-duration="700"></div>

			<span class="down-arrow">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/scroll-arrow.svg" data-aos="fade-up"></span>
			</span>
			<div class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" style="background-image: url('<?php echo $image['url']; ?>');"></div>
		</section>
	<?php endif; ?>
	
	<?php // start relationship partial
		$form = get_field('start_callback_form', 183);
		if( $form ): ?>
			<section class="relationships__start section start">
				<?php get_template_part( 'partials/partial', 'start' ); ?>
			</section>
	<?php endif; ?>
	
	<?php
		$contacts_title = get_field('contact_contacts_title');
		$contacts_intro = get_field('contact_contacts_intro');
	?>

	<section class="contact__contacts section">
		<?php if( $contacts_intro ): ?>
			<div class="bg darkgrey-bg"></div>
			<div class="contact__intro--intro content-container padding addheight">
				<div class="content-wrapper addheight">
					<?= ( $contacts_title )? '<h2 class="title page__title--single title" data-aos="fade-up">' . $contacts_title .'</h2>' : '';?>
					<?= ( $contacts_intro )? '<div class="contacts__intro--content content" data-aos="fade-up" data-aos-delay="100">' . $contacts_intro .'</div>' : '';?>
				</div>
			</div>
		<?php endif; ?>
		
		<?php if( have_rows('contact_team_members') ): ?>
			<ul class="grid four-column">
				<?php while( have_rows('contact_team_members') ): the_row(); 
					$image = get_sub_field('image');
					$name = get_sub_field('name');
					$jobtitle = get_sub_field('job_title');
					$tel = get_sub_field('telephone');
					$email = get_sub_field('email');
				?>
					<li class="grid-item" data-aos="grid-item">
						<div class="thumbnail-wrapper">
							<div class="thumbnail fade" style="background-image: url('<?php echo get_sub_field('image')['url']; ?>')"></div>
						</div>
						
						<div class="meta slide">
							<?= ( $name )? '<h3 class="post-title name">' . $name .'</h3>' : '';?>
							<p class="input">						
								<?= ( $jobtitle )? '<p class="cat jobtitle">' . $jobtitle .'</p>' : '';?>
								<?= ( $tel )? '<a class="cat tel" href="' . $tel['url'] .'">' . $tel['title'] . '</a>' : '';?>
								<?= ( $email )? '<a class="cat email" href="' . $email['url'] .'">' . $email['title'] . '</a>' : '';?>
							</p>
						</div>                        
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</section>

	<section class="contact__stories section stories">
		<?php get_template_part( 'partials/partial', 'blog' ); ?>
	</section>
</div>

<?php get_footer(); ?>
