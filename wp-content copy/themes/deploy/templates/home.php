<?php
/**
* Template Name: Home Page Template
* Description: Main template for the homepage of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page home">
    
    <section class="home__hero section">
        <div class="wrapper padding">
            <div class="home__hero--est est" data-aos="fade-in" data-aos-delay="150">
                <span>Est. <span class="est-icon"><img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/est-icon.svg" alt="Established 2010"></span> 2010</span>
            </div>
        
            <?php
                $title_one = get_field('hero_title_one');
                $title_two = get_field('hero_title_two');
                $intro = get_field('hero_intro');
            ?>
            
            <?= ( $title_one )? '<h1 class="home__hero--title one">' . $title_one .'</h1>' : '';?>
            <?= ( $title_two )? '<h1 class="home__hero--title two">' . $title_two .'</h1>' : '';?>
            <?= ( $intro )? '<div class="home__hero--intro" data-aos="fade-in" data-aos-delay="1000"><p class="content">' . $intro .'</p></div>' : '';?>
        
            <div class="home__hero--certs">
                <?php if( have_rows('hero_certificates') ): ?>
                	<ul class="grid">
                    	<?php while( have_rows('hero_certificates') ): the_row(); 
                    		$icon = get_sub_field('icon');
                    		?>
                    		<li class="grid-item" data-aos="grid-item" data-aos-delay="200">
                    			<?php if( $icon ): ?>
                    				<img class="cert-icon fade" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
                    			<?php endif; ?>
                    		</li>
                    	<?php endwhile; ?>
                	</ul>
                <?php endif; ?>
            </div>
        </div>
    </section>
    
    <?php // main featured image
        $image = get_field('featured_image');
    ?>
    <?php if( $image ): ?>
        <section class="home__featured-image section featured-image">
            <div class="bg grey-bg featured-image-bg" data-aos="slide-right" data-aos-duration="1000"></div>
            <span class="down-arrow" data-aos="fade-up">
                <img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/scroll-arrow.svg"></span>
            </span>
            <div class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        </section>
    <?php endif; ?>
    
    <section class="home__casestudies section addheight">
        <?php
            $title = get_field('retationships_title');
            $intro = get_field('relationships_intro');
            $link = get_field('relationships_link');
        ?>
        <div class="home__casestudies--intro content-container padding">
            <div class="content-wrapper">
                <?= ( $title )? '<h2 class="title addheight" data-aos="fade-up">' . $title .'</h2>' : '';?>
                <?= ( $intro )? '<p class="content" data-aos="fade-up">' . $intro .'</p>' : '';?>
                <?= ( $link )? '<a class="more-link" data-aos="fade-up" href="' . $link['url'] .'"><span>' . $link['title'] . '</span></a>' : '';?>
            </div>
        </div>

        <div class="home__casestudies--built">
            <?php if( have_rows('relationships_built') ): 
            	while( have_rows('relationships_built') ): the_row(); 
                    $title = get_sub_field('built_title');
            		$posts = get_sub_field('built_posts');
                    $built_link = get_sub_field('built_link');
            		?>
                    <div class="posts">
                        <?= ( $title )? '<div class="title-wrapper" data-aos="fade-up"><h2 class="title padding">' . $title .'</h2></div>' : '';?>
                        <?php 
                            $posts = get_sub_field('built_posts');
                            if( $posts ): ?>
                                <ul class="grid three-column">
                                <?php foreach( $posts as $post): ?>
                                    <?php setup_postdata($post); ?>
                                    <li class="grid-item" data-aos="grid-item">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="thumbnail-wrapper">
                                                <?php if ( has_post_thumbnail() ) { ?>
                                                    <div class="thumbnail fade" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
                                                <?php } else { ?>
                                                    <div class="thumbnail fade" style="background-image: url('<?php bloginfo('template_directory'); ?>/library/images/placeholder-landscape.png')"></div>
                                                    <?php } ?>
                                            </div>
                                        </a>
                                        <div class="meta slide">
                                            <div class="input">						
                                                <?php
                                                    $services = get_the_terms( $post->ID , 'services' );						
                                                    if ( $services != null ) {
                                                        foreach( $services as $service ) {
                                                            $service_link = get_term_link( $service, 'services' );
                                                ?>
                                                        <?php echo '<a class="cat" href="' . $service_link . '">' . $service->name . '</a>';?>
                                                <?php unset($service); } } ?>
                                            </div>
                                            <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                                <?= ( $built_link )? '<a class="more-link" data-aos="fade-up" href="' . $built_link['url'] .'"><span>' . $built_link['title'] . '</span></a>' : '';?>
                                </ul>
                                <?php wp_reset_postdata(); ?>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
    
    <section class="home__services--services services-partial section">
        <?php get_template_part( 'partials/partial', 'services' ); ?>
    </section>
    
    <section class="home__contact contact-partial section">
        <?php get_template_part( 'partials/partial', 'contact' ); ?>
    </section>
    
    <section class="home__customers section">
        <div class="wrapper padding">
            <?php
                $title = get_field('customers_title');
            ?>
            <?php if( have_rows('customer_icons') ): ?>
                <?= ( $title )? '<div class="title-wrapper" data-aos="fade-up"><h2 class="title">' . $title . '</h2></div>' : '';?>
            	<ul class="home__customers--icons grid five-column">
                	<?php while( have_rows('customer_icons') ): the_row(); 
                    		$icon = get_sub_field('icon');
                		?>
                        <?= ( $icon )? '<li class="grid-item"><img class="customer-logo" data-aos="fade-up" src="' . $icon['url'] . '"></li>' : '';?>
                	<?php endwhile; ?>
            	</ul>
            <?php endif; ?>
        </div>
    </section>
    
    <section class="home__stories section stories">
        <?php get_template_part( 'partials/partial', 'blog' ); ?>
    </section>
</div>

<?php get_footer(); ?>