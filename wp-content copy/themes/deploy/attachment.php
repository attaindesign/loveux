<?php
/**
* The template for displaying attachments
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

	<section class="page blog">
        <article class="blog-attachment">
            <div class="blog-attachment__image">
                <?php echo wp_get_attachment_image( get_the_ID(), 'large' ); ?>                
            </div>
            <h1 class="page-title page-title__attachment"><?php the_title(); ?></h1>
            <div class="blog-attachment__content">
                <?php the_content(); ?>
            </div>            
        </article>
	</section>

<?php get_footer(); ?>
