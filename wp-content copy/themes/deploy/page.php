<?php
/**
* The template for displaying pages
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages and that
* other "pages" on your WordPress site will use a different template.
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

	<section class="page wrapper">
        <h1 class="page-title page-title__default"><?php the_title(); ?></h1>
		<?php get_template_part( 'partials/loop', 'default' ); ?>
	</section>

<?php get_footer(); ?>
