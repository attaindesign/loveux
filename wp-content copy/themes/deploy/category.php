<?php
/**
* The template for displaying category pages
*
* Used to display category-type pages if nothing more specific matches a query.
* For example, puts together date-based pages if no date.php file exists.
*
* If you'd like to further customize these category views, you may create a
* new template file for each one. For example, tag.php (Tag archives),
* category.php (Category archives), author.php (Author archives), etc.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

	<section class="page blog">
        <div class="blog-category">
			<div class="content-container padding addheight">
				<div class="content-wrapper addheight">
	            	<h1 class="title page-title__category"><?php the_archive_title(); ?></h1>
	            	<div class="blog-category__description content">
	                	<?php echo the_archive_description(); ?>                
	            	</div>
				</div>
			</div>         
			
	        <?php get_template_part( 'partials/loop', 'blog' ); ?>
	        <?php get_template_part( 'partials/content', 'pagination' ); ?>   
        </div>
	</section>

<?php get_footer(); ?>
