<?php
/**
* The template for displaying all single 'vacancies' posts and attachments.
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

	<section class="page vacancies">
		<?php get_template_part( 'partials/content', 'single-vacancies' ); ?>
	</section>

<?php get_footer(); ?>