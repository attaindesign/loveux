<?php
/**
* Template Name: Case Studies Archive Template (custom posts)
* Description: Main template for the case studies posts archive of theme
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

<div class="page casestudies">

	<div class="bg grey-bg slidebg" data-aos="slide-down" data-aos-delay="200" data-aos-duration="1000"></div>
	<section class="casestudies__archive section">
		<div class="casestudies__archive--intro content-container padding addheight">
			<div class="content-wrapper">
				<?php
					$title = get_field('casestudies_archive_title', 331);
					$intro = get_field('casestudies_archive_intro', 331);
				?>
				<?= ( $title )? '<h1 class="title addheight casestudies__archive--title" data-aos="fade-up">' . $title .'</h1>' : '';?>
				<?= ( $intro )? '<div class="casestudies__archive--content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
			</div>
		</div>          

		<div class="casestudies__archive--built">
			<?php get_template_part( 'partials/loop', 'casestudies' ); ?>
		</div>
		
		<div class="pagination" data-aos="fade-in">
			<?php get_template_part( 'partials/content', 'pagination' ); ?>
		</div>
	</section>
	
	<?php 
		$form = get_field('start_callback_form', 183);
		if( $form ): ?>
			<section class="casestudies__start section start">
				<?php get_template_part( 'partials/partial', 'start' ); ?>
			</section>
	<?php endif; ?>
	
	<section class="casestudies__stories section stories">
		<?php get_template_part( 'partials/partial', 'blog' ); ?>
	</section>

</div>

<?php get_footer(); ?>