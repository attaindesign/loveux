<?php
/**
 * The template used for displaying contact partial
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<?php
    $contact_text = get_field('contact_info', 'option');
    $tel = get_field('telephone', 'option');
    $email = get_field('email', 'option');
    $callback = get_field('callback_link', 'option');
?>

<div class="contact-content-container content-container padding">
    <div class="content-wrapper" data-aos="fade-up">
        <?php if( $contact_text ): ?>
            <p class="content"><?= $contact_text; ?></p>
            <p><a class="tel link" href="<?= $tel['url']; ?>"><?= $tel['title']; ?></a></p>
            <p><a class="email link" href="<?= $email['url']; ?>"><?= $email['title']; ?></a></p>
            <a class="callback more-link" href="<?= $callback['url']; ?>"><span><?= $callback['title']; ?></span></a>
        <?php endif; ?>
    </div>
</div>