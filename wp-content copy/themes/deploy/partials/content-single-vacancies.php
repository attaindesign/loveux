<?php
/**
 * The template part for displaying single 'vacancies' custom posts
 *
 * @package WordPress
 * @subpackage attain
 *
 */
?>

<div class="page vacancies">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<article class="vacancies__single">
    	
            <?php
                $intro = get_field('single_vacancy_description');
                $extra = get_field('single_vacancy_skills_extra_content');
                $apply_link = get_field('single_vacancy_skills_link');
            ?>
            
    		<section class="vacancies__single--intro section padding">			
                <div class="content-container addheight">
                    <div class="content-wrapper addheight">
                        <h1 class="title vacancies__single--title"><?php the_title(); ?></h1>
                        <?= ( $intro )? '<div class="vacancies__single--content content">' . $intro .'</div>' : '';?>
                    </div>
                </div>  
            </section>
            
            <section class="vacancies__single--skills section padding">
                <?php if( have_rows('single_vacancy_skills') ): ?>
                	<ul class="skills-list">
                    	<?php while( have_rows('single_vacancy_skills') ): the_row(); 
                    		$skill = get_sub_field('skill');
                    		?>
                    		<li class="skills-list-item"><?php echo $skill; ?></li>
                    	<?php endwhile; ?>
                	</ul>
                <?php endif; ?>
            </section>
            
            <section class="vacancies__single--apply section padding">
                <div class="content-container addheight">
                    <div class="content-wrapper addheight">
                        <?= ( $extra )? '<div class="vacancies__single--content content">' . $extra .'</div>' : '';?>
                        <?= ( $apply_link )? '<a class="more-link" href="' . $apply_link['url'] . '">' . $apply_link['title'] .'</a>' : '';?>
                    </div>
                </div>  
            </section>
            
            
    	</article>
    <?php endwhile; endif; ?>
</div>
