<?php
/**
 * The template used for displaying page loop content
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<?php if(have_posts()){ ?>
	<?php while(have_posts()){ the_post(); ?>
		<article class="page__post">
			<div class="page__content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php } ?>
<?php } ?>