<?php
/**
 * The template used for displaying sart relationship partial
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<?php
    $title = get_field('start_title', 183);
    $intro = get_field('start_intro', 183);
    $form = get_field('start_callback_form', 183);
    $tel = get_field('telephone', 'option');
    $email = get_field('email', 'option');
?>    
    
<?php if( $form ): ?>
    <div class="start-partial padding">
        <?= ( $title )? '<h2 class="title" data-aos="fade-up">' . $title . '</h2>' : '';?>
        <?= ( $intro )? '<div class="intro" data-aos="fade-up">' . $intro .'</div>' : '';?>
        <?= ( $tel )? '<a class="tel" data-aos="fade-up" href="' . $tel['url'] .'">' . $tel['title'] . '</a>' : '';?>
        <?= ( $email )? '<a class="email" data-aos="fade-up" href="' . $email['url'] .'">' . $email['title'] . '</a>' : '';?>
        
        <?php if( get_field('start_callback_form', 183) ): ?>
            <div class="start-partials form" data-aos="fade-up">
                <?php Ninja_Forms()->display( get_field('start_callback_form', 183) ); ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
