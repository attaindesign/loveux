<?php
/**
 * The template used for displaying blog loop content
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<div class="title-wrapper" data-aos="fade-up">
    <h2 class="title padding">Stories and Insights</h2>
</div>

<ul class="grid blog-partial four-column">
    <?php
    	$args = array( 'numberposts' => '4' );
    	$recent_posts = wp_get_recent_posts( $args );
    	foreach( $recent_posts as $recent ){
            $permalink = get_permalink($recent['ID']);
            $image_url = wp_get_attachment_url( get_post_thumbnail_id($recent['ID']) );
            $title = get_the_title($recent['ID']);
            $category = get_the_category($recent['ID']);
            $category_link = get_category_link( $category[0]->term_id );
            ?>
            
            <li class="grid-item" data-aos="grid-item">
                <a href="<?php echo $permalink ?>">
                    <div class="thumbnail-wrapper">
                      <div class="thumbnail fade" style="background-image: url('<?php echo $image_url ?>')"></div>
                    </div>
                </a>
                    
                <div class="meta slide">                    
                    <span class="cat">
                        <?php
                        if ( ! empty( $category ) ) {
                            echo '<a href="' . $category_link . '">';
                                echo esc_html( $category[0]->name );
                            echo '</a>';
                        }
                        ?>
                    </span>
                    
                    <h3 class="post-title"><a href="<?php echo $permalink ?>"><?php echo $title ?></a></h3>
                    <a class="more-link" href="<?php echo $permalink ?>">Find out more</a>
                </div>
            </li>
    	
        <?php } ?>

    <?php wp_reset_query(); ?>
</ul>
