<?php
/**
 * The template used for displaying 'case studies' loop content
 *
 * @package WordPress
 * @subpackage attain
 *
 */
?>

<?php if(have_posts()){ ?>
	<ul class="grid three-column">
		<?php while(have_posts()){ the_post(); ?>
			<article class="casestudies__post grid-item" data-aos="grid-item">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<div class="thumbnail-wrapper">
						<?php if ( has_post_thumbnail() ) { ?>
							<div class="thumbnail fade" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
						<?php } else { ?>
							<div class="thumbnail fade" style="background-image: url('<?php bloginfo('template_directory'); ?>/library/images/placeholder-landscape.png')"></div>
						<?php } ?>
					</div>
					<div class="meta slide">
						<div class="tag">						
							<?php
								$services = get_the_terms( $post->ID , 'services' );						
								if ( $services != null ) {
									foreach( $services as $service ) {
										$service_link = get_term_link( $service, 'services' );
							?>
							<?php echo '<a class="cat" href="' . $service_link . '">' . $service->name . '</a>';?>
							<?php unset($service); } } ?>
						</div>
						<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</div>
				</a>
			</article>
		<?php } ?>
	</ul>
<?php } ?>

