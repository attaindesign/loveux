<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage attain
 *
 */
?>

<div class="page blog">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article class="blog__single">
			<section class="blog__single--intro section padding">
				<div class="title-wrapper" data-aos="fade-up"><h1 class="title page__title--single title"><?php the_title(); ?></h1></div>
			</section>

			<section class="blog__single--share share-partial section padding">
				<?php get_template_part( 'partials/partial', 'share' ); ?>
			</section>
			
			<?php // main image
				$image = get_field('post_featured_image');
			?>
			<?php if( $image ): ?>
				<section class="blog__single--featured-image section featured-image">
					<div class="bg grey-bg featured-image-bg" data-aos="slide-right" data-aos-duration="1000"></div>
					<span class="down-arrow" data-aos="fade-up">
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/scroll-arrow.svg"></span>
					</span>
					<div class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" style="background-image: url('<?php echo $image['url']; ?>');"></div>
				</section>
			<?php endif; ?>
			
			<?php // post content 
				if( have_rows('post_content_sections') ):
				$count = 0;
			?>
				<section class="blog__single--postcontent">
					<ul class="grid one-column">
						<?php while( have_rows('post_content_sections') ): the_row(); 
							$title = get_sub_field('post_content_sections_title');
							$content = get_sub_field('post_content_sections_content');
							$image = get_sub_field('post_content_sections_image');
						?>
							<li class="grid-item section">
								<div class="content-container padding">
									<div class="content-wrapper" data-aos="fade-up">
										<?= ( $title )? '<h2 class="title">' . $title .'</h2>' : '';?>
										<?= ( $content )? '<p class="content">' . $content .'</p>' : '';?>
									</div>
								</div>
								<?= ( $image )? '<div class="post-image" data-aos="fade-up"><img src="' . $image['url'] . '"/></div>' : '';?>
							</li>
							
							<?php // increment count and if 2 rows have been shown, display quote.
								$count++;
								if ($count == 2) {
							?>
							<?php
								$text_quote = get_field('post_quote_text');
								$author_quote = get_field('post_quote_author');
							?>
								<?php if( $text_quote ): ?>
									<section class="quote section">
										<div class="quote-content-container padding">
											<div class="content">
												<div class="quote-bracket left" data-aos="fade-up">
													<img src="<?php echo get_template_directory_uri(); ?>/library/images/quote-bracket-left.svg" />
												</div>
												<div class="content-wrapper">
													<?= ( $text_quote )? '<p class="text" data-aos="fade-in" data-aos-delay="100">' . $text_quote .'</p>' : '';?>
													<div class="author-wrapper">
														<?= ( $author_quote )? '<p class="author" data-aos="fade-in" data-aos-delay="150">' . $author_quote .'</p>' : '';?>
													</div>
												</div>
												<div class="quote-bracket right" data-aos="fade-up">
													<img src="<?php echo get_template_directory_uri(); ?>/library/images/quote-bracket-right.svg" />
												</div>
											</div>
										</div>
									</section>
								<?php endif; ?>
						   	<?php } ?>
						<?php endwhile; ?>
					</ul>
				</div>
			<?php endif; ?>
		</article>
		
		<section class="blog__single--share share-partial section">
			<?php get_template_part( 'partials/partial', 'share' ); ?>
		</section>
		
		<section class="blog__single--contact contact-partial section">
			<?php get_template_part( 'partials/partial', 'contact' ); ?>
		</section>
		
		<div class="blog__single--author-bio author-bio padding" data-aos="fade-up">
			<?php 					
				$author_id = get_the_author_meta('ID');
			?>
			
			<div class="author-header">
				<div class="author-content">
					
					<div class="author-avatar">
						<?php echo get_avatar( get_the_author_meta( 'user_email' )); ?>
					</div>
					
					<div class="author-title">
						<h4><?php the_author_meta( 'display_name' ); ?></h4>

						<?php // social icons
							if( have_rows('author_social_links', 'user_'. $author_id) ): ?>
							<ul class="author-social">
								<?php while( have_rows('author_social_links', 'user_'. $author_id) ): the_row(); 
									$social = get_sub_field('social');
									$link = get_sub_field('link');
								?>
									<li class="item">
										<a href="<?= $link['url']; ?>" class="social-icons <?= $social; ?> <?= $social; ?>-icon">
										<span><?= $social; ?></span></a>
									</li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>

					<div class="author-description">
						<?php
						$authordesc = get_the_author_meta( 'description' );
						if ( ! empty ( $authordesc ) ) { ?>
							<p class="content">
								<?php echo wpautop( $authordesc ); } ?>
							</p>
					</div>
				</div>
			</div>
		</div>

		<section class="blog__single--stories section stories">
			<?php get_template_part( 'partials/partial', 'blog' ); ?>
		</section>
	<?php endwhile; endif; ?>

</div>
