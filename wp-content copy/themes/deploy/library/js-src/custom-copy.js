// /**
//  * Custom Scripts
//  *
//  * Contains custom scripts for the website
//  *
//  * @package WordPress
//  * @subpackage Deploy
//  *
//  */
// 
// (function(d,w,$) {
// 
//     /* EXPANDS PARENT TO HEIGHT OF ABSOLUTE CHILDREN */
// 
//     // intro wrapper heights
//      function introHeight() {
//        $('.content-container .content-wrapper').each(function (i, el) { // child
//          $(el).closest(".content-container").css({ 'height': $(el).height() + 5}); // parent
//        });
//      }
//      introHeight();
// 
//       function introRelHeight() {
//         $('.intro-rel .wrapper').each(function (i, el) { // child
//           $(el).closest(".intro-rel").css({ 'height': $(el).height() + 5}); // parent
//         });
//       }
//       introRelHeight();
// 
//     // title wrapper heights
//     function titleHeight() {
//       $('.title-wrapper .title').each(function (i, el) { // child
//         $(el).closest(".title-wrapper").css({ 'height': $(el).height() + 7}); // parent
//       });
//     }
//     titleHeight();
// 
// 
//     /* EXPANDS ABSOLUTE GREYBG TO HEIGHT OF CHILDREN */
// 
//     /* standard bg height */
//     var bgHeight = 0;
//     $('.addheight').each(function(){ bgHeight+=$(this).height() + 164});
// 
//     $('.bg').each(function(){
//         $(this).height(bgHeight);
//     });
// 
//     /* featured-image bg height */
//     var featuredBGheight = 0;
//     $('.featured-image, .addHeight').each(function(){ featuredBGheight+=$(this).height() + 200});
// 
//     $('.featured-image-bg').each(function(){
//         $(this).height(featuredBGheight);
//     });
// 
//     /* extra image bg height */
//     var extrabgHeight = 0;
//     $('.extra-image-two').each(function(){ extrabgHeight+=$(this).height() + 75});
// 
//     $('.extrabg').each(function(){
//         $(this).height(extrabgHeight);
//     });
// 
// 
//     /* FORCES HEIGHT OF THUMBNAIL-WRAPPER for all grids */
// 
//     /* keeps four-column items square across device widths */
//     var fourItemWidth = $(".four-column .grid-item").width();
//     $(".four-column .thumbnail-wrapper").height(fourItemWidth);
// 
//     /* global three-column grid-item widths */    
//     var threeItemWidth = $('.three-column .grid-item').width();
//     $(".three-column .thumbnail-wrapper").height(threeItemWidth - 100);
// 
//     /* ADDS CLASS TO PARENT MENU ITEM */
// 
//     $('.head__nav').find('li:has(ul)').addClass('parent');
// 
// 
//     /* MENU TOGGLES */
// 
//     $('.burger-btn').click( function() {
//         $(".burger-btn").toggleClass("transform");
//         $(".nav-logo").toggleClass("logo-toggle");
//         $(".menu-wrapper").toggleClass("toggle");
//     } );
// 
//     $('.parent').click( function() {
//         $(".parent").toggleClass("toggleSubMenu");
//     } );
// 
//     // animate on scroll
//     $(function () {
//         var os = new OnScreen({
//             tolerance: 100,
//             debounce: 0,
//             container: window
//         });
// 
//         os.on('enter', '.section', function (element, event) {
//             element.classList.add("animate");
//         }),
//         os.on('enter', '.one-column .grid-item', function (element, event) {
//             element.classList.add("one-column-item");
//         }),
//         os.on('enter', '.three-column .grid-item', function (element, event) {
//             element.classList.add("three-column-item");
//         }),
//         os.on('enter', '.four-column .grid-item', function (element, event) {
//             element.classList.add("four-column-item");
//         }),
//         os.on('enter', '.five-column .grid-item', function (element, event) {
//             element.classList.add("five-column-item");
//         }),
//         os.on('enter', '.relationships__single--main .gallery .item', function (element, event) {
//             element.classList.add("gallery-animate");
//         }),
//         os.on('enter', '.relationships__single--main .mobile-grid', function (element, event) {
//             element.classList.add("mobile-grid-animate");
//         }),
//         os.on('enter', '.casestudies__single--main .gallery .item', function (element, event) {
//             element.classList.add("gallery-animate");
//         }),
//         os.on('enter', '.casestudies__single--main .mobile-grid', function (element, event) {
//             element.classList.add("mobile-grid-animate");
//         }),
//         os.on('enter', '.down-arrow ', function (element, event) {
//             element.classList.add("fadeIn");
//         }),
//         os.on('enter', '.menu-footer', function (element, event) {
//             element.classList.add("animate");
//         });
//     });
// 
// })(document, window, jQuery);