/**
 * Custom Scripts
 *
 * Contains custom scripts for the website
 *
 * @package WordPress
 * @subpackage Deploy
 *
 */

(function(d,w,$) {
    
    /* EXPANDS PARENT TO HEIGHT OF ABSOLUTE CHILDREN */
    
    // intro wrapper heights
     function introHeight() {
       $('.content-container .content-wrapper').each(function (i, el) { // child
         $(el).closest(".content-container").css({ 'height': $(el).height() + 5}); // parent
       });
     }
     introHeight();
     
      function introRelHeight() {
        $('.intro-rel .wrapper').each(function (i, el) { // child
          $(el).closest(".intro-rel").css({ 'height': $(el).height() + 5}); // parent
        });
      }
      introRelHeight();
      
    // title wrapper heights
    function titleHeight() {
      $('.title-wrapper .title').each(function (i, el) { // child
        $(el).closest(".title-wrapper").css({ 'height': $(el).height() + 7}); // parent
      });
    }
    titleHeight();
    
    
    /* EXPANDS ABSOLUTE GREYBG TO HEIGHT OF CHILDREN */
    
    /* standard bg height */
    var bgHeight = 0;
    $('.addheight').each(function(){ bgHeight+=$(this).height() + 164});
    
    $('.bg').each(function(){
        $(this).height(bgHeight);
    });
    
    /* featured-image bg height */
    var featuredBGheight = 0;
    $('.featured-image, .addHeight').each(function(){ featuredBGheight+=$(this).height() + 200});
    
    $('.featured-image-bg').each(function(){
        $(this).height(featuredBGheight);
    });
    
    /* extra image bg height */
    var extrabgHeight = 0;
    $('.extra-image-two').each(function(){ extrabgHeight+=$(this).height() + 75});
    
    $('.extrabg').each(function(){
        $(this).height(extrabgHeight);
    });
    
    
    /* FORCES HEIGHT OF THUMBNAIL-WRAPPER for all grids */
    
    /* keeps four-column items square across device widths */
    var fourItemWidth = $(".four-column .grid-item").width();
    $(".four-column .thumbnail-wrapper").height(fourItemWidth);
    
    /* global three-column grid-item widths */    
    var threeItemWidth = $('.three-column .grid-item').width();
    $(".three-column .thumbnail-wrapper").height(threeItemWidth - 100);
    
    
    /* ADDS CLASS TO PARENT MENU ITEM */
    $('.head__nav').find('li:has(ul)').addClass('parent');
    
    
    /* MENU TOGGLES */
    $('.burger-btn').click( function() {
        $(".head__left--navigation").toggleClass("toggle");
    } );

    $('.parent').click( function() {
        $(".parent").toggleClass("toggleSubMenu");
    } );


    /* ANIMATE ON SCROLL */
    $(function() {
        AOS.init({
          // // Global settings:
          disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
          startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
          initClassName: 'aos-init', // class applied after initialization
          animatedClassName: 'aos-animate', // class applied on animation
          useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
          disableMutationObserver: false, // disables automatic mutations' detections (advanced)
          debounceDelay: 0, // the delay on debounce used while resizing window (advanced)
          throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
          
          // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
          offset: 100, // offset (in px) from the original trigger point
          // delay: 0, // values from 0 to 3000, with step 50ms
          duration: 500, // values from 0 to 3000, with step 50ms
          easing: 'ease', // default easing for AOS animations
          once: true, // whether animation should happen only once - while scrolling down
          mirror: false, // whether elements should animate out while scrolling past them
          anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
        });
    });

})(document, window, jQuery);