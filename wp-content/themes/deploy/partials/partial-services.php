<?php
/**
 * The template used for displaying services pages partial
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<?php
    $servicestitle = get_field('services_title', 2);
    $servicesintro = get_field('services_intro', 2);
?>

<div class="grey-bg"></div>
<div class="content-container padding">
    <div class="content-wrapper">
        <?= ( $servicestitle )? '<h2 class="title addheight" data-aos="fade-up">' . $servicestitle .'</h2>' : '';?>

        <div class="wrapper">
            <div class="est" data-aos="fade-up" data-aos-delay="100">
                <span>Est. <span class="est-icon"><img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/est-icon.svg" alt="Established 2010"></span> 2010</span>
            </div>
            <?= ( $servicesintro )? '<p class="content" data-aos="fade-up" data-aos-delay="200">' . $servicesintro .'</p>' : '';?>
        </div>
    </div>
</div>


<div class="pages">
    <?php 
        $posts = get_field('services_posts', 2);

    if( $posts ): ?>
        <ul class="grid four-column">
        <?php foreach( $posts as $post): ?>
            <?php setup_postdata($post); ?>
            <li class="grid-item" data-aos="grid-item">
                <a href="<?php the_permalink(); ?>">
                    <div class="thumbnail-wrapper">
                        <div class="thumbnail fade" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
                    </div>
                </a>
                <div class="meta slide">
                    <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="excerpt">
                        <?php the_excerpt(); ?>
                    </div>
                    <a class="more-link" href="<?php the_permalink(); ?>">Find out more</a>
                </div>
            </li>
        <?php endforeach; ?>
        </ul>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
    
</div>
