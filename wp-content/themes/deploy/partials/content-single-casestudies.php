<?php
/**
 * The template part for displaying single 'case studies' custom posts
 *
 * @package WordPress
 * @subpackage attain
 *
 */
?>

<div class="page casestudies">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article class="casestudies__single">
		
		<section class="casestudies__single--intro section padding">			
			<div class="title-wrapper" data-aos="fade-up"><h1 class="title page__title--single title"><?php the_title(); ?></h1></div>

			<?php // intro
			if( have_rows('customer_info') ): 
				while ( have_rows('customer_info') ): the_row();
					$test = get_sub_field('customer_name_test');
					$website = get_sub_field('customer_website');
			?>
			
			<?= ( $test )? '<p>' . $test .'</p>' : '';?>
			
				<ul class="customer-info grid three-column">
					<?= ( $website )? '<li class="grid-item website" data-aos="fade-up" data-aos-delay="450"><p class="label">Customer website</p><p class="input"><a href="' . $website['url'] . '" target="' . $website['target'] . '">' . $website['title'] . '</a></p></li>' : '';?>

					<li class="grid-item services" data-aos="fade-up" data-aos-delay="300">
						<p class="label">Services</p>
							<?php // servcies categories
								$services = get_the_terms( $post->ID , 'services' );						
								if ( $services != null ) {
									foreach( $services as $service ) {
										$service_link = get_term_link( $service, 'services' );
							?>
									<?php echo '<p class="input"><a href="' . $service_link . '">' . $service->name . '</a></p>' ;?>
							<?php unset($service); } } ?>
					</li>
					
					<li class="grid-item markets" data-aos="fade-up" data-aos-delay="600">
						<p class="label">Target Markets</p>
							<?php // channels (previously technologies) tags
								$markets = get_the_terms( $post->ID , 'Markets' );						
								if ( $markets != null ) {
									foreach( $markets as $market ) {
										$market_link = get_term_link( $market, 'Markets' );
							?>
									<?php echo '<p class="input"><a href="' . $market_link . '">' . $market->name . '</a></p>' ;?>
							<?php unset($market); } } ?>
					</li>
					
					<li class="grid-item channels" data-aos="fade-up" data-aos-delay="600">
						<p class="label">Channels</p>
							<?php // channels (previously technologies) tags
								$channels = get_the_terms( $post->ID , 'Technologies' );						
								if ( $channels != null ) {
									foreach( $channels as $channel ) {
										$channel_link = get_term_link( $channel, 'Technologies' );
							?>
									<?php echo '<p class="input"><a href="' . $channel_link . '">' . $channel->name . '</a></p>' ;?>
							<?php unset($channel); } } ?>
					</li>
					
				</ul>
			<?php
				endwhile;
				endif;
			?>
		</section>
		
		<?php // main image
			$image = get_field('main_image');
		?>
		<?php if( $image ): ?>
			<section class="casestudies__single--featured-image section featured-image">
				<div class="bg grey-bg featured-image-bg" data-aos="slide-right" data-aos-duration="1000"></div>
				<span class="down-arrow" data-aos="fade-up">
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/scroll-arrow.svg"></span>
				</span>
				<div class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" style="background-image: url('<?php echo $image['url']; ?>');"></div>
			</section>
		<?php endif; ?>
		
		<?php
			// details
			$heading = get_field('details_heading');
			$description = get_field('details_description');
			$conversion = get_field('details_conversion_rate');
			$value = get_field('details_order_value');
			$purchases = get_field('details_unique_purchases');
		?>
	
		<section class="casestudies__single--details section height">
			<div class="bg grey-bg"></div>

			<?php if(get_field('details_heading') || get_field('details_description')): ?>
						
				<div class="content-container padding addheight">
					<div class="content-wrapper">
						<?= ( $heading )? '<h2 class="title" data-aos="fade-up">' . $heading .'</h2>' : '';?>
						<?= ( $description )? '<p class="content" data-aos="fade-up">' . $description .'</p>' : '';?>
					</div>
				</div>
			<?php endif; ?>

			<?php
			if ( have_rows( 'details_rate_conversions' ) ) :
				while ( have_rows( 'details_rate_conversions' ) ) : the_row();
			?>
				<ul class="grid rates padding three-column addheight">
				<?php if ( have_rows( 'item' ) ) :
				   while ( have_rows( 'item' ) ) : the_row();
					   $label = get_sub_field( 'label' );
					   $rate = get_sub_field( 'rate' );
				   ?>
						<?= ( $rate )? '<li class="grid-item" data-aos="grid-item"><p class="input fade">' . $rate .'</p><p class="label fade">' . $label .'</p></li>' : '';?>
					<?php endwhile; endif; ?>
				</ul>
			<?php endwhile; endif; ?>
			
		    <ul class="gallery grid three-column">
				<?php // gallery images
					$images = get_field('details_images');
					if( $images ): ?>
			        <?php foreach( $images as $image ): ?>
			            <li class="grid-item" data-aos="grid-item">
							<div class="image fade" style="background-image: url('<?php echo $image['url']; ?>');"></div>
						</li>
			        <?php endforeach; ?>
				<?php endif; ?>
		    </ul>
		</section>	
		
		<section class="casestudies__single--main section">
			<?php // main content
				$main_heading = get_field('main_content_heading_one');
				$main_description = get_field('main_content_section_one');
			?>
			
			<?php if(get_field('main_content_heading_one') || get_field('main_content_section_one')): ?>
				<div class="content-container padding">	
					<div class="content-wrapper">
						<?= ( $main_heading )? '<h2 class="title" data-aos="fade-up">' . $main_heading .'</h2>' : '';?>
						<?= ( $main_description )? '<p class="content" data-aos="fade-up">' . $main_description .'</p>' : '';?>
					</div>
				</div>
			<?php endif; ?>
			
			<?php
				// mockup gallery images
				$laptop_mockup = get_field('main_content_laptop');
				$full_screenshot_one = get_field('main_content_full_page_one');
				$full_screenshot_two = get_field('main_content_full_page_two');
				$mobile_mockup = get_field('main_content_mobile');
				$extra_image_one = get_field('main_content_extra_image_one');
				$video = get_field('main_content_video');
				
				// branding colours
				$primary = get_field('primary');
				$secondary = get_field('secondary');
			?>
			
			<?php if(get_field('main_content_laptop') || get_field('main_content_full_page_one') || get_field('main_content_mobile') || get_field('main_content_full_page_two') || get_field('main_content_extra_image_one') || get_field('main_content_video') ): ?>
				
				<ul class="gallery list one-column">			
					<?= ( $laptop_mockup )? '<li class="item"><div class="laptop" data-aos="slide-left" data-aos-duration="700" style="background-color:' . $primary . '"><img data-aos="fade-in" data-aos-delay="900" data-aos-duration="700" src="' . $laptop_mockup['url'] . '" /></div></li>' : '';?>
					
					<?= ( $full_screenshot_one )? '<li class="item"><div class="screenshot screenshot-one" data-aos="slide-right" data-aos-duration="700" style="background-color:' . $secondary . '"><img data-aos="fade-in" data-aos-delay="800" data-aos-duration="700" src="' . $full_screenshot_one['url'] . '" /></div></li>' : '';?>
				
					<?= ( $full_screenshot_two )? '<li class="item"><div class="screenshot screenshot-two"><img data-aos="fade-in" src="' . $full_screenshot_two['url'] . '" /></div></li>' : '';?>
					
					<?= ( $extra_image_one )? '<li class="item"><div class="extra-image extra-image-one"><img data-aos="fade-in" src="' . $extra_image_one['url'] . '" /></div></li>' : '';?>
					
					<?= ( $video )? '<li class="item"><div class="video" data-aos="fade-in">' . $video . '</div></li>' : '';?>				
				</ul>
			<?php endif; ?>
			
			<div class="grid mobile-grid">	
				<?= ( $mobile_mockup )? '<div class="mobile"><img data-aos="fade-in" src="' . $mobile_mockup['url'] . '" /></div>' : '';?>
				
				<?php
					$main_heading_two = get_field('main_content_heading_two');
					$main_description_two = get_field('main_content_section_two');
				?>
				
				<?php if(get_field('main_content_heading_two') || get_field('main_content_section_one')): ?>
					<div class="intro-rel">	
						<div class="wrapper">
							<?= ( $main_heading_two )? '<h2 class="title" data-aos="fade-up">' . $main_heading_two .'</h2>' : '';?>
							<?= ( $main_description_two )? '<p class="content" data-aos="fade-up">' . $main_description_two .'</p>' : '';?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			
			<?php 
				$mobile_images = get_field('main_content_mobile_screenshots');
				if( $mobile_images ): ?>
				<div class="mobile-screenshots">
				    <ul class="gallery grid three-column">
				        <?php foreach( $mobile_images as $mobile_image ): ?>
				            <li class="grid-item" data-aos="grid-item">
								<div class="image fade" style="background-image: url('<?php echo $mobile_image['url']; ?>');"></div>
							</li>
				        <?php endforeach; ?>
				    </ul>
				</div>
			<?php endif; ?>
		</section>
		
		<?php // design
			if(get_field('info_graphics')): ?>
			<section class="casestudies__single--design section">
				<?php 
					if(get_field('design_heading')): 
					$design_heading = get_field('design_heading');
				?>
					<?= ( $design_heading )? '<div class="title-wrapper" data-aos="fade-up"><h2 class="title padding">' . $design_heading .'</h2></div>' : '';?>
				<?php endif; ?>
				
				<?php // info graphics
					$design_images = get_field('info_graphics');
					if( $design_images ): ?>
					<div class="info-graphics">
					    <ul class="gallery grid three-column">
					        <?php foreach( $design_images as $design_image ): ?>
					            <li class="grid-item" data-aos="grid-item">
									<div class="image fade" style="background-image: url('<?php echo $design_image['url']; ?>');"></div>
								</li>
					        <?php endforeach; ?>
					    </ul>
					</div>
				<?php endif; ?>
			</section>
		<?php endif; ?>
		
		<?php // extra image one
			$extra_image_two = get_field('main_content_extra_image_two');
		?>
		<?php if(get_field('main_content_extra_image_two') ): ?>
			<section class="casestudies__single--extraimage extraimage section">
				<div class="bg grey-bg extrabg" data-aos="fade-in"></div>
				<ul class="grid one-column">
					<?= ( $extra_image_two )? '<li class="item" data-aos="fade-in"><div class="extra-image extra-image-two"><img src="' . $extra_image_two['url'] . '" /></div></li>' : '';?>
				</ul>
			</section>
		<?php endif; ?>
		
		<?php 
			if(get_field('feedback_comments')):
			// feedback
			$contact_name = get_field('feedback_name');
			$comments = get_field('feedback_comments');
			$customer_logo = get_field('feedback_logo');
		?>
			<section class="casestudies__single--quote quote section padding">
				
				<div class="quote-content-container">
					<div class="content">						
						<div class="content-wrapper">
							<?= ( $comments )? '<p class="text" data-aos="fade-in" data-aos-delay="100">' . $comments .'</p>' : '';?>
							<div class="author-wrapper">
								<?= ( $contact_name )? '<p class="author" data-aos="fade-in" data-aos-delay="150">' . $contact_name .'</p>' : '';?>
								<?= ( $customer_logo )? '<div class="logo" data-aos="fade-in" data-aos-delay="200"><img src="' . $customer_logo['url'] . '" /></div>' : '';?>				
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>
	</article>
	
	<section class="casestudies__single--contact contact-partial section">
		<?php get_template_part( 'partials/partial', 'contact' ); ?>
	</section>
	
	<section class="casestudies__casestudies section casestudies">
        <?php get_template_part( 'partials/partial', 'casestudies' ); ?>
	</section>
<?php endwhile; endif; ?>
</div>
