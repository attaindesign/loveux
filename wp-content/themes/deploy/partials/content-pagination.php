<?php
/**
 * The template part for displaying pagination
 *
 * @package WordPress
 * @subpackage attain
 *
 */
?>

<?php
	$prev_link = get_previous_posts_link(__('Next'));
	$next_link = get_next_posts_link(__('Previous'));
	if ($prev_link || $next_link) {
		echo '<ul class="pagination-list list">';
		if ($next_link){
		echo '<li class="pagination__prev item"><span>'.$next_link .'</span></li>';
		}
		if ($prev_link){
		echo '<li class="pagination__next item"><span>'.$prev_link .'</span></li>';
		}
        echo '</ul>';
	}
 ?>

<?php wp_link_pages(); ?>
