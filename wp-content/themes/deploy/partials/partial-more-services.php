<?php
/**
 * The template used for displaying more services link partial
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<?php
    $moreservices_title = get_field('more_services_title');
    $moreservices_text = get_field('more_services_intro');    
    if( $moreservices_title || $moreservices_text ):
?>
    <div class="grey-bg"></div>
    <div class="content-container padding">
        <div class="content-wrapper">
            <?= ( $moreservices_title )? '<h2 class="title addheight" data-aos="fade-up">' . $moreservices_title .'</h2>' : '';?>

            <div class="wrapper">
                <div class="est" data-aos="fade-up" data-aos-delay="100">
                    <span>Est. <span class="est-icon"><img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/est-icon.svg" alt="Established 2010"></span> 2010</span>
                </div>
                <?= ( $moreservices_text )? '<div class="content" data-aos="fade-up" data-aos-delay="200">' . $moreservices_text .'</div>' : '';?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php 
    $moreservices_links = get_field('more_services_page_links');
    if( $moreservices_links ):
?>
    <div class="pages">
        <ul class="grid">
        	<?php foreach( $moreservices_links as $p ): ?>
                <?php setup_postdata($p); ?>
                <a href="<?php the_permalink(); ?>" class="grid-item" data-aos="grid-item">
                    <div class="text-wrapper">
                        <?php echo get_the_title( $p->ID ); ?>
                    </div>
                </a>
                <?php wp_reset_postdata(); ?>
        	<?php endforeach; ?>
    	</ul>
    </div>
<?php endif; ?>

<style type="text/css">
    .more-services-partial .grid-item {
        border-color: #000 !important;
        border-color: <?php the_field('si_bg_color'); ?> !important;
    }
    
    .more-services-partial .grid-item:hover {
        color: #fff;
        background-color: #000 !important;
        border-color: #000 !important;
        background-color: <?php the_field('si_bg_color'); ?> !important;
        border-color: <?php the_field('si_bg_color'); ?> !important;
    }
</style>