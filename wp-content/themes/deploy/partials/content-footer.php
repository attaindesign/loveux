<?php
/**
 * The template part for displaying site footer content
 *
 * @package WordPress
 * @subpackage attain
 *
 */
?>

<div class="foot wrapper">
    
    <?php // social icons
        if( have_rows('social_links', 'option') ): ?>
        <ul class="foot__social">
            <?php while( have_rows('social_links', 'option') ): the_row(); 
                $social = get_sub_field('social', 'option');
                $link = get_sub_field('link', 'option');
                $class = get_sub_field('social_class', 'option');
            ?>
                <li class="item">
                    <a href="<?= $link['url']; ?>" class="social-icons <?= $social; ?> <?= $social; ?>-icon fade">
                    <span><?= $social; ?></span></a>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
    
    <div class="foot__details">
        <?php // footer newsletter 
            if( have_rows('footer_newsletter', 'option') ): 
            while( have_rows('footer_newsletter', 'option') ): the_row();     		
                $footertitle = get_sub_field('footer_newsletter_title');
                $footerid = get_sub_field('footer_newsletter_id');
            ?>
                <div class="foot__details--newsletter footer-newsletter">
                    <?= ( $footertitle )? '<h3 class="title">' . $footertitle .'</h3>' : '';?>
                    <?php Ninja_Forms()->display( get_sub_field('footer_newsletter_id', 'option') ); ?>
                </div>
            <?php
                endwhile;
                endif;
            ?>
        
        <?php // callback
            if( have_rows('foot_callback', 'option') ): 
        	while( have_rows('foot_callback', 'option') ): the_row();     		
        		$title = get_sub_field('callback_title');
        		$text = get_sub_field('callback_text');
        		?>
        		<div class="foot__details--callback">
                    <?= ( $title )? '<h3 class="title">' . $title .'</h3>' : '';?>
                    <?= ( $text )? '<p class="text content">' . $text .'</p>' : '';?>
        		</div>
            <?php
                endwhile;
                endif;
            ?>
        
        <?php // contact details
            if( have_rows('foot_contact_details', 'option') ): 
            while( have_rows('foot_contact_details', 'option') ): the_row();     		
                $tel = get_sub_field('telephone');
                $email = get_sub_field('email');
                $address = get_field('address', 'option');
            ?>
                <div class="foot__details--contact">
                    <ul class="list">
                        <li class="item">    
                            <h3 class="title">Phone</h3>
                            <?= ( $tel )? '<a class="tel link" href="' . $tel['url'] .'">' . $tel['title'] . '</a>' : '';?>
                        </li>    
                        <li class="item">    
                            <h3 class="title">Mail Us</h3>
                            <?= ( $email )? '<a class="email link" href="' . $email['url'] .'">' . $email['title'] . '</a>' : '';?>
                        </li>
                        <li class="item">
                            <?= ( $address )? '<p class="address">' . $address .'</p>' : '';?>
                        </li>
                    </ul>
                </div>
            <?php
                endwhile;
                endif;
            ?>
    </div>
    
    <div class="foot__info">
        <?php // copyright 
            if( have_rows('foot_copyright', 'option') ): 
            while( have_rows('foot_copyright', 'option') ): the_row();     		
                $text = get_sub_field('text');
            ?>
                <div class="foot__info--copyright">
                    <?= ( $text )? '<p class="text content">' . $text .'</p>' : '';?>
                </div>
            <?php
                endwhile;
                endif;
            ?>

            <?php // page links 
                if ( have_rows( 'foot_page_links', 'option' ) ) : ?>
                <div class="foot__info--links">
                    <ul class="list">
                		<?php while ( have_rows( 'foot_page_links', 'option' ) ) : the_row();
                			if ( have_rows( 'links' ) ) : ?>
                			       <?php
                			       while ( have_rows( 'links' ) ) : the_row();
                				       $page = get_sub_field( 'page_link' );
                			       ?>
                                           <li class="item">
                                               <a href="<?php echo $page['url']; ?>"><?php echo $page['title']; ?></a>
                				            </li>
                			       <?php endwhile; ?> 
                		    <?php endif;
                        endwhile; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php // awards
                if ( have_rows( 'foot_awards', 'option' ) ) : ?>
                <div class="foot__info--awards">
                    <ul class="list">
                        <?php while ( have_rows( 'foot_awards', 'option' ) ) : the_row();
                            if ( have_rows( 'icon' ) ) : ?>
                                   <?php
                                   while ( have_rows( 'icon' ) ) : the_row();
                                       $image = get_sub_field( 'image' );
                                   ?>
                                       <li class="item">
                                           <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                                        </li>
                                    <?php endwhile; ?>
                            <?php endif;
                        endwhile; ?>
                    </ul>
                </div>
            <?php endif; ?>
    </div>
</div>

<?php wp_link_pages(); ?>
