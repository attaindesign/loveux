<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage attain
 * 
 */
?>

<?php
    $intro = get_field('services_intro', 2);
?>

<div class="title-wrapper padding" data-aos="fade-up"><h2 class="title page__title--single title">A few relationships we've built</h2></div>

<div class="pages">    
    <?php 
        $posts = get_posts(array(
        	'posts_per_page'	=> 4,
        	'post_type'			=> 'casestudies',
            'orderby'=>'rand'
        ));
        if( $posts ): ?>
            <ul class="grid four-column">
    	        <?php foreach( $posts as $post ):          
                    setup_postdata( $post );
    	        ?>
                    <li class="grid-item" data-aos="grid-item">
                        <a href="<?php the_permalink(); ?>">
                            <div class="thumbnail-wrapper">
                                <div class="thumbnail fade" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
                            </div>
                        </a>
                        <div class="meta slide">
                            <p class="input">						
                                <?php
                                    $services = get_the_terms( $post->ID , 'services' );						
                                    if ( $services != null ) {
                                        foreach( $services as $service ) {
                                            $service_link = get_term_link( $service, 'services' );
                                ?>
                                        <?php echo '<a class="cat" href="' . $service_link . '">' . $service->name . '</a>';?>
                                <?php unset($service); } } ?>
                            </p>
                            <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        </div>
    		        </li>
                <?php endforeach; ?>
    	    <?php wp_reset_postdata(); ?>
        <?php endif; ?>
</div>
