<?php
/**
* Template Name: Vacancies Archive Page Template (custom post)
* Description: Main template for the vacancies page of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page vacancies">
    
    <section class="vacancies__intro section">
        <div class="vacancies__intro--intro content-container padding">
            <div class="content-wrapper">
                <?php
                    $title = get_field('vacancies_intro_title', 609);
                    $intro = get_field('vacancies_intro_intro', 609);
                    $link = get_field('vacancies_intro_link', 609);
                ?>
                <?= ( $title )? '<h1 class="title vacancies__intro--title" data-aos="fade-up">' . $title .'</h1>' : '';?>
                <?= ( $intro )? '<div class="vacancies__intro--content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
                <?= ( $link )? '<a class="more-link" data-aos="fade-up" data-aos-delay="200"  href="' . $link['url'] .'"><span>' . $link['title'] . '</span></a>' : '';?>
            </div>
        </div>   
    </section>
    
    <?php
        $image = get_field('vacancies_intro_featured_image', 609);
    ?>
    <?php if( $image ): ?>
        <section class="vacancies__single--featured-image section featured-image">
            <div class="bg orange-bg featured-image-bg" data-aos="slide-right" data-aos-delay="200" data-aos-duration="700"></div>
            <div class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        </section>
    <?php endif; ?>
    
    <section class="vacancies__current section">
        <div class="vacancies__current--intro content-container padding">
            <div class="content-wrapper">
                <?php
                    $title = get_field('vacancies_current_title', 609);
                    $intro = get_field('vacancies_current_intro', 609);
                    $link = get_field('vacancies_current_link', 609);
                ?>
                <?= ( $title )? '<h1 class="title vacancies__current--title" data-aos="fade-up">' . $title .'</h1>' : '';?>
                <?= ( $intro )? '<div class="vacancies__current--content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
                <?= ( $link )? '<a class="more-link email" data-aos="fade-up" data-aos-delay="200" href="' . $link['url'] .'"><span>' . $link['title'] . '</span></a>' : '';?>
            </div>
        </div>
        
		<?php 
			$posts = get_posts(array(
				'post_type'	=> 'vacancies',
			));
            
            $loop = new WP_Query( $posts );
            
            while ( $loop->have_posts() ) : $loop->the_post();
                $id = get_the_ID();     
            
			if( $posts ): ?>
	        <div class="vacancies__current--list" data-aos="fade-in">        
                <ul class="grid one-column">
                    <?php foreach( $posts as $post ):          
                        setup_postdata( $post );
                    ?>
                        <li class="grid-item" data-aos="fade-up">
                            <div class="meta" >
                                <h3 class="post-title"><?php the_title(); ?></h3>
                                <?php 
                                    $excerpt = get_field('vacancy_list_excerpt');
                                ?>
                                <div class="excerpt">
                                    <?= ( $excerpt )? '<div class="content">' . $excerpt .'</div>' : '';?>
                                </div>
								<a class="post-link more-link" rel="<?php echo $id; ?>" href="#<?php echo $id; ?>">More</a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
				</ul>
                
				<div class="post-content">
					<button class="close-btn"><span><img class="img" src="<?php echo get_template_directory_uri(); ?>/library/images/icons/close.svg"></span></button>
					<h3 class="apply-title">Apply for position</h3>
                    
					<div id="single-post-container">
                    
                        <?php
                            $post = $_GET['postid'];
                            
                            $include = get_posts("include=$post");
                        ?>
                        
                            <h3 class="post-title"><?php the_title(); ?></h3>
                        
                    </div>
				</div>
	        </div>
			<?php 
                endif;
                endwhile;
            ?>
    </section>
</div>

<?php get_footer(); ?>