<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-main" element.
*
* @package WordPress
* @subpackage Deploy
*
*/
?>
<!doctype html>
<!--
      ____             __
   _ / __ \___  ____  / /___  __  __ ®
  (_) / / / _ \/ __ \/ / __ \/ / / /
 _ / /_/ /  __/ /_/ / / /_/ / /_/ /
(_)_____/\___/ .___/_/\____/\__, /
            /_/            /____/

    Thanks for your interest in the source code.
    Please feel free to have a look around to see how things work, but please don't steal.
    If you like what we do then please get in touch!

    www.deploy.co.uk

    Copyright (c) <?php echo date('Y');?>  :Deploy®. All Rights Reserved.
-->
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Language" content="en-GB"/>
    
    <?php wp_enqueue_script('jquery'); ?>
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i|Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
    
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />


    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'ua-code-here', 'auto');
    ga('send', 'pageview');
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164798627-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-164798627-1');
</script> 
</head>
<body role="document">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <header class="head" role="banner">
        <?php
            $tel = get_field('telephone', 'option');
            $email = get_field('email', 'option');
            $address = get_field('address', 'option');
        ?>
        
        <div class="head__left">
            <div class="head__left--navigation">
                <a class="burger-btn" data-aos="fade-in">
                    <span class="line one"></span>
                    <span class="line two"></span>
                    <span class="line three"></span>
                </a>
                
                <div class="nav-container">
                    <div class="head__left--logo menu-logo" data-aos="fade-in">
                        <a href="<?php echo home_url(); ?>"></a>                        
                    </div>
                    
                    <?php header_menu(); ?>
                    
                    <div class="menu-footer" role="contentinfo">
                        <?php get_template_part( 'partials/content', 'menufooter' ); ?>    
                    </div>
                </div>
            </div>
            
            <div class="head__left--logo header-logo" data-aos="fade-in">
                <a href="<?php echo home_url(); ?>"></a>
            </div>
            
            <div class="head__left--contact mobile" data-aos="fade-in">
                <a class="tel" href="<?= $tel['url']; ?>"><img class="img" src="<?php echo get_template_directory_uri(); ?>/library/images/icons/telephone-icon.svg" alt="Email Deploy"></a>
                <a class="email" href="<?= $email['url']; ?>"><img class="img" src="<?php echo get_template_directory_uri(); ?>/library/images/icons/email-icon.svg" alt="Email Deploy"></a>
            </div>
        </div>
        
        <div class="head__right">
            <div class="head__right--contact desktop" data-aos="fade-in">
				<p>
                    <?= ( $tel )? '<a class="tel" href="' . $tel['url'] .'">' . $tel['title'] . '</a>' : '';?>
                    <?= ( $email )? '<a class="tel" href="' . $email['url'] .'">' . $email['title'] . '</a>' : '';?>
				</p>
            </div>
            <div class="head__right--address" data-aos="fade-in">
                <?= ( $address )? '<p class="address">' . $address .'</p>' : '';?>
            </div>
        </div>
    </header>
    
    <div id="fullpage" class="page__wrapper">           
        <main class="page__main" role="main">
