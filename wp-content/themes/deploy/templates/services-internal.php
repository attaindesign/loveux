<?php
/**
* Template Name: Services Internal Page Template
* Description: Main template for the homepage of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page services-internal">
    <section class="services-internal__single">
        
        <?php
            $intro = get_field('service_intro_text');
            if( $intro ): 
        ?>
            <section class="services-internal__single--intro content-container padding">
                <div class="content-wrapper">
                    <h1 class="title services-internal__single--title" data-aos="fade-up"><?php the_title(); ?></h1>
                    <?= ( $intro )? '<div class="services-internal__single-content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
                </div>
            </section>
        <?php endif; ?>
        
        <?php
            // main content
            $heading_main = get_field('service_main_content_heading');
            $text_main = get_field('service_main_content_text');
            $image_main = get_field('service_main_content_image');
            
            // top image / video
            $image = get_field('service_featured_image');
            $video = get_field('service_featured_video');
            
            if( $heading_main || $text_main || $image_main || $image || $video ):
        ?>

            <section class="services-internal__single--main one section addHeight">
                <?php if( $video ): ?>
                    <div class="services-internal__single--featured-image section featured-image" data-aos="fade-in" data-aos-duration="1000">
                        <video class="top-image" id="intro-video" src="<?php echo $video; ?>" muted autoplay loop playsinline></video>
                    </div>
                <?php elseif( $image ): ?>
                    <div class="services-internal__single--featured-image section featured-image" data-aos="fade-in" data-aos-duration="1000">
                        <img class="top-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                    </div>
                <?php endif; ?>
                
                <div class="content-container padding">
                    <div class="content-wrapper" data-aos="fade-up">
                        <?= ( $heading_main )? '<h2 class="title">' . $heading_main .'</h2>' : '';?>
                        <?= ( $text_main )? '<div class="content">' . $text_main .'</div>' : '';?>
                    </div>
                </div>

                <?php
                    // quote
                    if( have_rows('quote') ):
                        while( have_rows('quote') ): the_row();
                        $text_quote = get_sub_field('service_quote_text');
                        $author_quote = get_sub_field('service_quote_author');
                        
                        if ( $text_quote ) :
                ?>
                <div class="services__single--quote quote section">
                    <div class="quote-content-container padding">
                        <div class="content">                                    
                            <div class="content-wrapper">
                                <?= ( $text_quote )? '<p class="text" data-aos="fade-up" data-aos-delay="100">' . $text_quote .'</p>' : '';?>
                                <?= ( $author_quote )? '<p class="author" data-aos="fade-up" data-aos-delay="150">' . $author_quote .'</p>' : '';?>
                            </div>
                        </div>
                    </div>
                </div>    
                <?php
                    endif;
                    endwhile;
                    endif;
                ?>

                <?= ( $image_main )? '<div class="services-internal-image" data-aos="fade-up" data-aos-delay="300"><img src="' . $image_main['url'] .'" alt="' . $image_main['alt'] . '" /></div>' : '';?>
            </section>
        <?php endif; ?>
        
        <?php if( have_rows('secondary_content_sections') ): ?>
            <section class="services-internal__single--secondary two">
                <?php
                    while( have_rows('secondary_content_sections') ): the_row();
                    $heading_secondary = get_sub_field('secondary_heading');
                    $text_secondary = get_sub_field('secondary_text_content');
                    $image_secondary = get_sub_field('secondary_image');
                    $image_secondary_large = get_sub_field('secondary_image_large');
                ?>
                    <div class="content-container padding addHeight" data-aos="fade-up" data-aos-delay="300">
                            <?= ( $heading_secondary )? '<h2 class="title">' . $heading_secondary .'</h2>' : '';?>
                            <?= ( $text_secondary )? '<div class="content">' . $text_secondary .'</div>' : '';?>
                            <?= ( $image_secondary )? '<img class="floating-image" src="' . $image_secondary['url'] .'" alt="' . $image_secondary['alt'] . '" data-aos="fade-up" data-aos-delay="300" />' : '';?>
                    </div>
                    
                    <?= ( $image_secondary_large )? '<div class="large-image" data-aos="fade-up" data-aos-delay="300"><img src="' . $image_secondary_large['url'] .'" alt="' . $image_secondary_large['alt'] . '" /></div>' : '';?>
                <?php endwhile; ?>
                
                <div class="services-internal__single--contact contact-partial section">
                    <?php get_template_part( 'partials/partial', 'contact' ); ?>
                </div>
                
            </section>
            

        <?php endif; ?>
        
        <?php // contact form
            $post_form_title = get_field('post_contact_form_title');
            $post_form_id = get_field('post_contact_form');
            
            if( get_field('post_contact_form') ):
        ?>
            <section class="services-internal__single--contact-form">
                <?= ( $post_form_title )? '<h3 class="title">' . $post_form_title .'</h3>' : '';?>
                <?php Ninja_Forms()->display( get_field('post_contact_form') ); ?>
            </section>		
        <?php endif; ?>
        
        <section class="services-internal__single--more-services more-services-partial section">
            <?php get_template_part( 'partials/partial', 'more-services' ); ?>
        </section>
    </div>
</div>

<style type="text/css">
    .si-bg-color  {
        background-color: <?php the_field('si_bg_color'); ?>;
    }
    .si-bg-color .head__right--address p,
    .si-bg-color .head__right--contact a,
    .head__left--logo a:before,
    .services-internal__single--intro,
    .services-internal__single--intro a {
        color: #000;
        color: <?php the_field('si_text_color'); ?> !important;
    }
    .services-internal__single--intro a {
        border-color: <?php the_field('si_text_color'); ?> !important;
    }
    .burger-btn .line {
        background-color: <?php the_field('si_text_color'); ?> !important;
    }
</style>

<?php get_footer(); ?>