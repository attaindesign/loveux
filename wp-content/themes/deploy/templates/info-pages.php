<?php
/**
* Template Name: Info Pages Template
* Description: Main template for the info pages (terms, pivacy...) of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page info">

	<section class="info__intro section">
        <div class="info__intro--intro content-container padding">
            <div class="content-wrapper">
                <h1 class="title page__title--single title" data-aos="fade-up"><?php the_title(); ?></h1>
            </div>
        </div>
	</section>
    
    <?php if( have_rows('info_section') ): ?>
        <section class="info__items section">
            <?php while( have_rows('info_section') ): the_row(); 
    		    $heading = get_sub_field('info_heading');
    		    $subheading = get_sub_field('info_sub_heading');
    		    $content = get_sub_field('info_content');
	        ?>
                <div class="info__items--content content-container padding">
                    <?= ( $heading )? '<h2 class="section-title heading" data-aos="fade-up">' . $heading .'</h2>' : '';?>
                    <?= ( $subheading )? '<h3 class="section-title subheading" data-aos="fade-up" data-aos-delay="100">' . $subheading .'</h3>' : '';?>
                    <?= ( $content )? '<p class="content" data-aos="fade-up" data-aos-delay="100">' . $content .'</p>' : '';?>
                </div>
	       <?php endwhile; ?>
       </section>
      <?php endif; ?>
</div>

<?php get_footer(); ?>