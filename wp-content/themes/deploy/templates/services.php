<?php
/**
* Template Name: Services Single Page Template
* Description: Main template for the homepage of theme
*
* @package WordPress
* @subpackage Deploy
*
*/
get_header(); ?>

<div class="page services">
    <div class="services__single">
        <?php
            $intro = get_field('service_intro_text');
        ?>
        
        <section class="services__single--intro content-container padding">
            <div class="content-wrapper">
                <h1 class="title services__single--title" data-aos="fade-up"><?php the_title(); ?></h1>
                <?= ( $intro )? '<div class="services__single-content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
            </div>            
        </section>

        <?php if( have_rows('service_intro_certificates') ): ?>
            <div class="certs">
                <ul class="grid">
                    <?php while( have_rows('service_intro_certificates') ): the_row(); 
                    $icon = get_sub_field('icon');
                    ?>
                    <li class="grid-item" data-aos="grid-item" data-aos-delay="200">
                        <?php if( $icon ): ?>
                            <img class="cert-icon fade" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
                        <?php endif; ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>
        
        <?php
            $image = get_field('service_fetured_image');
        ?>
        
        <?php if( $image ): ?>
            <section class="services__single--featured-image section featured-image">
                <img class="image" data-aos="slide-left" data-aos-delay="500" data-aos-duration="1000" src="<?php echo $image['url']; ?>"></img>
            </section>
        <?php endif; ?>

        <?php if( have_rows('service_main_content_sections') ):?>
            <section class="services__single--main section addHeight">
                <?php 
                    while( have_rows('service_main_content_sections') ): the_row();
                    // one
                    $heading_one = get_sub_field('service_main_content_heading_one');
                    $text_one = get_sub_field('service_main_content_text_one');
                    $image_one = get_sub_field('service_main_content_image_one');
                    // two
                    $heading_two = get_sub_field('service_main_content_heading_two');
                    $text_two = get_sub_field('service_main_content_text_two');
                    $image_two_desktop = get_sub_field('service_main_content_image_two_desktop');
                    $image_two_mobile = get_sub_field('service_main_content_image_two_mobile');
                ?>
                
                    <?php if( $heading_one || $text_one || $image_one ): ?>
                        <div class="one">
                            <div class="content-container padding">
                                <div class="content-wrapper">
                                    <?= ( $heading_one )? '<h2 class="title" data-aos="fade-up">' . $heading_one .'</h2>' : '';?>
                                    <?= ( $text_one )? '<div class="content" data-aos="fade-up" data-aos-delay="100">' . $text_one .'</div>' : '';?>
                                </div>
                            </div>
                            <div class="services-image" data-aos="fade-up" data-aos-delay="300">
                                <img src="<?php echo $image_one['url']; ?>" />
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                        $text_quote = get_field('service_quote_text');
                        $author_quote = get_field('service_quote_author');    
                        if ( $text_quote ) :
                    ?>
                        <div class="services__single--quote quote section">
                            <div class="quote-content-container padding">
                                <div class="content">                                    
                                    <div class="content-wrapper">
                                        <?= ( $text_quote )? '<p class="text" data-aos="fade-up" data-aos-delay="100">' . $text_quote .'</p>' : '';?>
                                        <?= ( $author_quote )? '<p class="author" data-aos="fade-up" data-aos-delay="150">' . $author_quote .'</p>' : '';?>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    <?php
                        endif;
                    ?>

                    <?php // two
                    if( $heading_two || $text_two || $image_two_desktop || $image_two_mobile ): ?>
                        <div class="two">
                            <div class="content-container padding">
                                <div class="content-wrapper">
                                    <?= ( $heading_two )? '<h2 class="title" data-aos="fade-up">' . $heading_two .'</h2>' : '';?>
                                    <?= ( $text_two )? '<div class="content" data-aos="fade-up" data-aos-delay="100">' . $text_two .'</div>' : '';?>
                                </div>
                            </div>
                            <?php if ( $image_two_desktop ): ?>
                                <div class="services-image desktop" data-aos="fade-up">
                                    <img src="<?php echo $image_two_desktop['url']; ?>">
                                </div>
                            <?php endif; ?>
                            <?php if ( $image_two_mobile ): ?>
                                <div class="services-image mobile" data-aos="fade-up">
                                    <img src="<?php echo $image_two_mobile['url']; ?>"/>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    
                <?php endwhile; ?>
            </section>
        <?php endif; ?>

        
        <?php // contact form
            $post_form_heading = get_field('post_contact_form_heading');
            $post_form_intro = get_field('post_contact_form_intro');
            $post_form_tel = get_field('post_contact_form_telephone');
            $post_form_email = get_field('post_contact_form_email');
            $post_form_id = get_field('post_contact_form');
            
            if( get_field('post_contact_form') ):
        ?>
            <section class="services__single--contact-form" id="contact-form">
                <?= ( $post_form_heading )? '<h2 class="title" data-aos="fade-up">' . $post_form_heading .'</h2>' : '';?>
                <?= ( $post_form_intro )? '<p class="content" data-aos="fade-up">' . $post_form_intro .'</p>' : '';?>

                <div class="contact-form-links">
                    <?= ( $post_form_tel )? '<a class="link tel" data-aos="fade-up" href="' . $post_form_tel['url'] .'"><span>' . $post_form_tel['title'] . '</span></a>' : '';?>
                    <?= ( $post_form_email )? '<a class="link email" data-aos="fade-up" href="' . $post_form_email['url'] .'"><span>' . $post_form_email['title'] . '</span></a>' : '';?>
                </div>

                <?= ( $post_form_title )? '<h3 class="title">' . $post_form_title .'</h3>' : '';?>
                <?php Ninja_Forms()->display( get_field('post_contact_form') ); ?>
            </section>		
        <?php endif; ?>
        
        <section class="services__single--contact contact-partial section">
            <?php get_template_part( 'partials/partial', 'contact' ); ?>
        </section>
        
        <section class="services__single--services services-partial section">
            <?php get_template_part( 'partials/partial', 'services' ); ?>
        </section>

    </div>
</div>

<?php get_footer(); ?>