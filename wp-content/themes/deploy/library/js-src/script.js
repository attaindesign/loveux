function revealMessage() { // reveals message on click of button
    document.getElementById("hiddenMessage").style.display = 'block'; // chages style to display: block;
}

function countDown() {
    var currentVal = document.getElementById("countDownButton").innerHTML; // gets current value of the button by using innerHTML
    var newVal = 0; // create a new variable to store new value
    if (currentVal > 0) { // if the current button is more than 0
        newVal = currentVal - 1; // it counts down 1 and stores it to newVal - wont go below 0 and into negative numbers
    }
    document.getElementById("countDownButton").innerHTML = newVal;
    // add newVal to button using innerHTML
}