/**
 * Custom Scripts
 *
 * Contains custom scripts for the website
 *
 * @package WordPress
 * @subpackage Deploy
 *
 */

(function(d,w,$) {
    
    /* ----- EXPANDS PARENT TO HEIGHT OF ABSOLUTE CHILDREN ----- */
    
    // intro wrapper heights
     function introHeight() {
       $('.content-container .content-wrapper').each(function (i, el) {
         // $(el).closest(".content-container").css({ 'height': $(el).height() + 5});
       });
     }
     introHeight();
     
      function introRelHeight() {
        $('.intro-rel .wrapper').each(function (i, el) {
          $(el).closest(".intro-rel").css({ 'height': $(el).height() + 5});
        });
      }
      introRelHeight();
      
    // title wrapper heights
    function titleHeight() {
      $('.title-wrapper .title').each(function (i, el) {
        $(el).closest(".title-wrapper").css({ 'height': $(el).height() + 7});
      });
    }
    titleHeight();
    
    
    /* ----- EXPANDS ABSOLUTE GREYBG TO HEIGHT OF CHILDREN ----- */
    
    // standard bg height
    var bgHeight = 0;
    $('.addheight').each(function(){ bgHeight+=$(this).height() + 164});
    
    $('.bg').each(function(){
        $(this).height(bgHeight);
    });
    
    // featured-image bg height
    var featuredBGheight = 0;
    $('.featured-image, .addHeight').each(function(){ featuredBGheight+=$(this).outerHeight() + 200});
    
    $('.featured-image-bg').each(function(){
        $(this).height(featuredBGheight);
    });


    // extra image bg height
    var extrabgHeight = 0;
    $('.extra-image-two').each(function(){ extrabgHeight+=$(this).height() + 75});
    
    $('.extrabg').each(function(){
        $(this).height(extrabgHeight);
    });

    
    /* ----- FORCES HEIGHT OF THUMBNAIL-WRAPPER FOR ALL GRIDS ----- */
    
    // keeps four-column items square across device widths
    var fourItemWidth = $(".four-column .grid-item").width();
    $(".four-column .thumbnail-wrapper").height(fourItemWidth);
    
    // global three-column grid-item widths
    var threeItemWidth = $('.three-column .grid-item').width();
    $(".three-column .thumbnail-wrapper").height(threeItemWidth - 100);
    

    /* ----- MENU ----- */
    
    var logoWidth = $('.primary-menu ').width();
    $('.head__left--logo a').width(logoWidth);
    
    // adds class to parent menu items
    $('nav').find('li:has(ul)').addClass('parent');
    
    // add sequential classes to sub-menus
    $(function(){
        var i = 1;
        $('ul.wp-megamenu-sub-menu').addClass(function(idx) {
            return "sub-menu__" + idx;
        });
    });
    
    // adds close arrow to sub-menu slides
    $('.wp-megamenu-sub-menu > li:first-child').append('<div class="arrowClose"></div>');

    // open menu on click
    $('.burger-btn').click( function() {
        $('.head__left--navigation').toggleClass('toggle');
    } );
    
    // open sub-menu
    $('.parent').on('click', function() {
        $(this).children('.wp-megamenu-sub-menu').addClass('openSubMenu');        
        
        var $hideItems = $('.wp-megamenu > li').children('a');
        
        if ( $('.head__left--navigation').hasClass('toggle') ) {
            $('.burger-btn').click( function() {
                $('.wp-megamenu-sub-menu').removeClass('openSubMenu');
                
                $('.menu-logo a').attr('id', 'white');
                $('.toggle > .burger-btn').attr('id', 'white');
                $hideItems.removeClass('hideItems');
            } );
        }

        if ( $(this).children('.sub-menu__0').hasClass('openSubMenu') ) {
            $('.menu-logo a').attr('id', 'white');
            $('.toggle > .burger-btn').attr('id', 'white');
            $hideItems.addClass('hideItems');

        } else if ( $(this).children('.sub-menu__1').hasClass('openSubMenu') ) {
            $('.menu-logo a').attr('id', 'orange');
            $('.toggle > .burger-btn').attr('id', 'orange');
            $('.wp-megamenu > li').children('a').addClass('hideItems');
            $hideItems.addClass('hideItems');

        } else if ( $(this).children('.sub-menu__2').hasClass('openSubMenu') ) {
            $('.menu-logo a').attr('id', 'maroon');
            $('.toggle > .burger-btn').attr('id', 'maroon');
            $('.wp-megamenu > li').children('a').addClass('hideItems');
            $hideItems.addClass('hideItems');

        } else if ( $(this).children('.sub-menu__3').hasClass('openSubMenu') ) {
            $('.menu-logo a').attr('id', 'pink');
            $('.toggle > .burger-btn').attr('id', 'pink');
            $('.wp-megamenu > li').children('a').addClass('hideItems');
            $hideItems.addClass('hideItems');

        } else {
            $('.menu-logo a').attr('id', 'white');
            $('.toggle > .burger-btn').attr('id', 'white');
            
            setTimeout(function() {
                $hideItems.removeClass('hideItems');
            }, 150);
        }

        // close sub-menu (using sub-menu arrow)
        $(document).on('click', '.arrowClose', function () {
            $(this).closest('.wp-megamenu-sub-menu').removeClass('openSubMenu');
            
            $('.menu-logo a').attr('id', 'white');
            $('.toggle > .burger-btn').attr('id', 'white');
            
            setTimeout(function() {
                $hideItems.removeClass('hideItems');
            }, 150);
        });
    });
    
    
    /* ----- SERVICES INTERNAL ----- */
    
    if( $('.page').hasClass('services-internal') === true ) {
        $('body').addClass('si-bg-color');
    }    
    
    
    /* ----- SPOTIFY TABLE ----- */
    
    // currency icons - Total earnings
    $('<div class="currency-switcher"><span class="icon sterling-total"></span> <span class="icon dollar-total"></span> <span class="icon euro-total"> </span></div>').appendTo( $('.spotify-table thead .column-6') );
        
        $('<div class="currency-switcher"><span class="icon sterling-total"></span> <span class="icon dollar-total"></span> <span class="icon euro-total"> </span></div>').appendTo( $('.spotify-table thead .column-7') );
        
                $('<div class="currency-switcher"><span class="icon sterling-total"></span> <span class="icon dollar-total"></span> <span class="icon euro-total"> </span></div>').appendTo( $('.spotify-table thead .column-8') );

    // currency icons - Earnings per day
    $('<div class="currency-switcher"><span class="icon sterling-day"></span> <span class="icon dollar-day"></span> <span class="icon euro-day"> </span></div>').appendTo( $('.spotify-table thead .column-10') );
        
        $('<div class="currency-switcher"><span class="icon sterling-day"></span> <span class="icon dollar-day"></span> <span class="icon euro-day"> </span></div>').appendTo( $('.spotify-table thead .column-11') );
        
            $('<div class="currency-switcher"><span class="icon sterling-day"></span> <span class="icon dollar-day"></span> <span class="icon euro-day"> </span></div>').appendTo( $('.spotify-table thead .column-12') );
            
    // switcher
    $('.spotify-table .sterling-total').addClass('active');
    $('.spotify-table .sterling-day').addClass('active');

    $('.spotify-table .column-6').css("display","table-cell");
    $('.spotify-table .column-10').css("display","table-cell");

    // total earnings
    $('.spotify-table .sterling-total').click( function() {
        $('.column-7').fadeOut().css("display","none");
        $('.column-8').fadeOut().css("display","none");
        $('.dollar-total').removeClass('active');
        $('.euro-total').removeClass('active');
        
        $('.column-6').fadeIn().css("display","table-cell");
        $('.sterling-total').addClass('active');
    } );
    
    $('.spotify-table .dollar-total').click( function() {
        $('.column-6').fadeOut().css("display","none");
        $('.column-8').fadeOut().css("display","none");
        $('.spotify-table .sterling-total').removeClass('active');
        $('.spotify-table .euro-total').removeClass('active');
        
        $('.column-7').fadeIn().css("display","table-cell");
        $('.dollar-total').addClass('active');
    } );
    
    $('.spotify-table .euro-total').click( function() {
        $('.column-6').fadeOut().css("display","none");
        $('.column-7').fadeOut().css("display","none");
        $('.sterling-total').removeClass('active');
        $('.dollar-total').removeClass('active');

        $('.column-8').fadeIn().css("display","table-cell");
        $('.euro-total').addClass('active');
    } );
    
    // earnings per day
    $('.spotify-table .sterling-day').click( function() {
        $('.column-11').fadeOut().css("display","none");
        $('.column-12').fadeOut().css("display","none");
        $('.dollar-day').removeClass('active');
        $('.euro-day').removeClass('active');
        
        $('.column-10').fadeIn().css("display","table-cell");
        $('.sterling-day').addClass('active');
    } );
    
    $('.spotify-table .dollar-day').click( function() {
        $('.column-10').fadeOut().css("display","none");
        $('.column-12').fadeOut().css("display","none");
        $('.sterling-day').removeClass('active');
        $('.euro-day').removeClass('active');
    
        $('.column-11').fadeIn().css("display","table-cell");
        $('.dollar-day').addClass('active');
    } );
    
    $('.spotify-table .euro-day').click( function() {
        $('.column-10').fadeOut().css("display","none");
        $('.column-11').fadeOut().css("display","none");
        $('.sterling-day').removeClass('active');
        $('.dollar-day').removeClass('active');
    
        $('.column-12').fadeIn().css("display","table-cell");
        $('.euro-day').addClass('active');
    } );
    
    // load more buttons
    var totalRows = $('.spotify-table tr').length;
    if ( totalRows > 8 ) {
        $('.spotify-table').after('<div class="load-more"><input id="show" type="button" value="Load More" /></div>');
    }

    var totalrowshidden;
    var rows2display=8;
    var rem=0;
    var rowCount=0;
    var forCntr;
    var forCntr1;
    var MaxCntr=0;
    
    $('#hide').click(function() {
        var rowCount = $('.spotify-table tr').length;
        rowCount=rowCount/2;
        rowCount=Math.round(rowCount)
    
        for (var i = 0; i < rowCount; i++) {
            $('tr:nth-child('+ i +')').hide(300); 
        }                            
    });
    
    $('#show').click(function() {
        rowCount = $('.spotify-table tr').length;
        MaxCntr=forStarter+rows2display;
    
        if (forStarter<=$('.spotify-table tr').length) {
            for (var i = forStarter; i < MaxCntr; i++) {
             $('tr:nth-child('+ i +')').show(200);
            }
            forStarter=forStarter+rows2display
        } else {
            $('#show').hide();
        }
    });
    
    var rowCount = $('.spotify-table tr').length;
    for (var i = $('.spotify-table tr').length; i > rows2display; i--) {
        rem=rem+1
        $('tr:nth-child('+ i +')').hide(200);
    }
    
    forCntr=$('.spotify-table tr').length-rem;
    forStarter=forCntr+1


    /* ----- ANIMATE ON SCROLL ----- */
    
    $(function() {
        AOS.init({
          // // Global settings:
          disable: false,
          startEvent: 'DOMContentLoaded',
          initClassName: 'aos-init',
          animatedClassName: 'aos-animate',
          useClassNames: false,
          disableMutationObserver: false,
          debounceDelay: 0,
          throttleDelay: 99,
          
          // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
          offset: 100,
          duration: 500,
          easing: 'ease',
          once: true,
          mirror: false,
          anchorPlacement: 'top-bottom',
        });
    });

})(document, window, jQuery);