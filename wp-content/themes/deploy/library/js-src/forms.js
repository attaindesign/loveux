/**
 * Forms
 *
 * Contains custom scripts for the forms aspect of the website
 *
 * @package WordPress
 * @subpackage attain
 *
 */
 (function(d, w, $) {
     
     // Polyfill for 'includes' object
     if (!String.prototype.includes) {
         String.prototype.includes = function(search, start) {
             if (typeof start !== 'number') {
                 start = 0;
             }

             if (start + search.length > this.length) {
                 return false;
             } else {
                 return this.indexOf(search, start) !== -1;
             }
         };
     }
     
     function inputSomething() {
         $('.nf-field, .field').each(function(index, item){
             
             var labelHeight = $(this).find('.nf-field-label label, .label').height();
             var parentItem = $(this).parent().prop('className');

             if(parentItem.includes('listcheckbox-container') || parentItem.includes('submit-container')){} else {
                 $(this).find('.nf-field-label, .label').css('padding-top', labelHeight);
             }                                    
         }); 
                        
         $('.nf-field, .field').click(function(e) {
             $(this).find('.nf-field-label').addClass('this-is-active');               
             $(e.srcElement).change(function(element) {
                 if ($(this).val() > 0) {                 
                     $(this).parent().parent().addClass('active-label');
                     $(this).parent().parent().removeClass('error-label');
                 } else {
                     $(this).parent().parent().removeClass('active-label');
                     $(this).parent().parent().addClass('error-label');
                 }
             });                                   
         });
         
         $(document).mouseup(function(e) {
             var container = $('.nf-field, .field');
             
             container.each(function(index, item){
                 if(!$(this).find('input').val() > 0){
                 
                 if (!$(this).is(e.target) && $(this).has(e.target).length === 0) {
                     $(this).find('.nf-field-label, .label').removeClass('this-is-active');
                 }
             }
                 
             });                                    
         }); 
     }
     
     
     $(document).ready(function(e){
         inputSomething()
     });
     $(document).on('nfFormReady', function(e, layoutView) {
        inputSomething()
     });

 })(document, window, jQuery);