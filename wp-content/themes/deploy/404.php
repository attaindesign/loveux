<?php
/**
* The template for displaying 404 pages (not found)
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

    <div class="page error-404">
    	<h1 class="page-title page-title__error-404">404</h1>
    	<a class=btn "error-404__btn" href="/">Go Home</a>
    </div>

<?php get_footer(); ?>