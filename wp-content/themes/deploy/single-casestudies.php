<?php
/**
* The template for displaying all single 'casestudies' posts and attachments.
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

	<section class="page casestudies">
		<?php get_template_part( 'partials/content', 'single-casestudies' ); ?>
	</section>

<?php get_footer(); ?>