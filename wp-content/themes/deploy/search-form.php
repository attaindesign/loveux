<?php
/**
* Template for displaying search forms in Theme
*
* @package WordPress
* @subpackage Deploy
* 
*/
?>

<form role="form" action="/" method="get" class="search-form">
	<div role="search" class="search-form__container">
		<input class="search-form__input" type="text" aria-label="Search" name="s" id="s" value="<?php the_search_query(); ?>" placeholder="Search and Hit Enter">
	</div>
</form>