<?php
// 
//       ____             __
//    _ / __ \___  ____  / /___  __  __ ®
//   (_) / / / _ \/ __ \/ / __ \/ / / /
//  _ / /_/ /  __/ /_/ / / /_/ / /_/ /
// (_)_____/\___/ .___/_/\____/\__, /
//             /_/            /____/
// 
//     Thanks for your interest in the source code.
//     Please feel free to have a look around to see how things work, but please don't steal.
//     If you like what we do then please get in touch!
// 
//     www.deploy.co.uk
// 
//     Copyright (c) 2018  :Deploy®. All Rights Reserved.

/**
* Functions
*
* Custom functions, support, custom post types and more.
*
* @category   Deploy
* @package    Wordpress
* @subpackage Deploy
* @author     Deploy
* @copyright  Deploy
* @link       https://www.deploy.co.uk
*/


/*------------------------------------*\
	Theme Support
\*------------------------------------*/

/**
* Global content width
*/
if ( ! isset( $content_width ) ) $content_width = 1133;

if (function_exists('add_theme_support')) {
    // Support Featured Images
    add_theme_support( 'post-thumbnails' );
    // Create Proper WordPress Titles
    add_theme_support( 'title-tag' );
    add_post_type_support( 'page', 'excerpt' );
    add_post_type_support( 'post', 'excerpt' );
    add_post_type_support( 'casestudies', 'thumbnail' );
    add_post_type_support( 'vacancies', 'thumbnail' );
}
    
    
/*------------------------------------*\
	Walker
\*------------------------------------*/

// Building A Simple WordPress Walker For Cleaner Nav Menu Markup
/* class Clean_Walker_Nav_Menu extends Walker_Nav_Menu {
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '<ul class="sub-menu"><div class="sub-menu-logo" data-aos="fade-in"><a href="<?php echo home_url(); ?>"></a></div><div class="sub-wrapper">';
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '</div></ul>';
    }

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    $classes = array(
        'sub-menu',
        'menu-depth-' . $display_depth
    );
    $class_names = implode( ' ', $classes );

    // build html
    if ($display_depth == 1) {
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    } else {
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }

        $active_class = '';
        if( in_array('current-menu-item', $classes) ) {
            $active_class = ' class="active"';
        } else if( in_array('current-menu-parent', $classes) ) {
            $active_class = ' class="active-parent"';
        } else if( in_array('current-menu-ancestor', $classes) ) {
            $active_class = ' class="active-ancestor"';
        }

        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }

        $output .= '<li'. $active_class . '><a href="' . $url . '">' . $item->title . '</a></li>';
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '</li>';
    }
}
*/

/*------------------------------------*\
	Functions
\*------------------------------------*/

/**
 * Setup the theme image sizes
 */
function image_sizes_thumbs() {
    add_image_size('large', 700, '', true);
    add_image_size('medium', 250, '', true);
    add_image_size('small', 120, '', true);
    // Create Custom Thumbnail Size
    add_image_size( 'custom-thumbnail', 250, 250, true );
}

/**
 * Settings for wp_nav_menu()
 *
 * In header_menu
 */
function header_menu() {
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'head__nav_inner',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'depth'           => 0,
		// 'walker'          => new Clean_Walker_Nav_Menu(),
        'container'       => false,
        'items_wrap'      => '<nav class="head__nav" role="navigation"><ul class="primary-menu"><div class="menu-wrapper">%3$s</div></ul></nav>'
		)
	);
}

/**
 * Settings for wp_nav_menu()
 *
 * In footer_menu
 */
function footer_menu() {
	wp_nav_menu(
	array(
		'theme_location'  => 'footer-menu',
		'menu'            => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'depth'           => 0,
		//'walker'          => new Clean_Walker_Nav_Menu(),
        'container'       => false,
        'items_wrap'      => '<nav class="foot__nav" role="navigation"><ul>%3$s</ul></nav>'
		)
	);
}

/** 
 * Include navigation menu
 */
function menus() {
	register_nav_menus(
		array(
		  'header-menu' => __( 'Header Menu' ),
		  'footer-menu' => __( 'Footer Menu' )
		)
	);
}

/**
 * Register and/or Enqueue
 *
 * Styles for the theme
 */
function add_styles() {
    wp_register_style('parent-style', get_template_directory_uri() . '/library/css/style.css', array(), '1.0', 'all');
    wp_enqueue_style('parent-style');
}

/**
 * Register and/or Enqueue
 *
 * Scripts for the theme (Set to 'true' to locate JS in footer)
 */
function addJs() {
    // Enqueue AOS styles (Animate On Screen)
    wp_enqueue_style(' AOS_animate', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css', false, null);
    wp_enqueue_script('AOS', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js', false, null, true);

    wp_register_script( 'all-scripts', get_stylesheet_directory_uri() . '/library/js/all.min.js', array(), '1.0', true );
	wp_enqueue_script( 'all-scripts' );
}

wp_localize_script( 'ajax-pagination', 'ajaxpagination', array(
	'ajaxurl' => admin_url( 'admin-ajax.php' )
));

/**
 * Async
 *
 * Conform with Google PageSpeed Insights
 */
function async_scripts($url) {
    if ( strpos( $url, '#asyncload') === false )
        return $url;
    else if ( is_admin() )
        return str_replace( '#asyncload', '', $url );
    else
	return str_replace( '#asyncload', '', $url )."' async='async"; 
}
add_filter( 'clean_url', 'async_scripts', 11, 1 );

/**
 * Remove p tags from Images
 *
 * to clean up markup
 */
 function filter_ptags_on_images($content) {
     return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
 }

/**
 * Redirect
 *
 * any user trying to access comments page
 */
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}

/**
 * Registers a widget area
 *
 * Called Primary sidebar
 */
function my_register_sidebars() {
	register_sidebar(
		array(
			'id' => 'primary',
			'name' => __( 'Primary' ),
			'description' => __( 'This is a sidebar for your Blog (if supported)' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
}

/**
 * Blacklist WordPress menu class names
 *
 * Making a cleaner navigation
 */
 add_filter('nav_menu_css_class', 'discard_menu_classes', 10, 2);

 function discard_menu_classes($classes, $item) {
    $classes = array_filter(
        $classes,
            create_function( '$class',
                'return in_array( $class,
                    array( "current-menu-item", "current-menu-parent" ) );' )
        );
        return array_merge(
        $classes,
            (array)get_post_meta( $item->ID, '_menu_item_classes', true )
        );
    }

/**
 * Modify Excerpt Length
 */
function custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/*------------------------------------*\
	Deploy WP Mods admin area
\*------------------------------------*/

/**
 * Modify Admin Footer Text
 */
function change_footer_admin () {
	echo 'Designed and built by <a href="https://www.deploy.co.uk/">Deploy</a>';
}

/**
 * Changing the title from "Powered by WordPress"
 */
function put_my_title(){
	return ('Deploy');
}

/**
 * Link details of image go in here
 */
function put_my_url(){
	$currentURL = 'https://www.deploy.co.uk/';
	return $currentURL;
}

/**
 * Insert Custom Login Logo
 */
function my_login_logo() {
	wp_register_style('admin', get_template_directory_uri() . '/library/css/admin.css', array(), '1.0', 'all');
    wp_enqueue_style('admin'); // Enqueue it!
}

/**
 * Change the footer text on the login page
 */
function back_to_site_text( $translated ) {
    if ( in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ){
        $translated = str_ireplace(  '&larr; Back to %s', '&larr; Return to site',  $translated );
    }
    return $translated;
}

/**
 * Remove toolbar items
 */
function annointed_admin_bar_remove() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
}

/**
 * Remove All Dashboard Widgets
 */
function remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
}

/**
 * Remove All Yoast HTML Comments
 *
 * https://gist.github.com/paulcollett/4c81c4f6eb85334ba076
 */
if (defined('WPSEO_VERSION')){
    add_action('get_header',function (){ ob_start(function ($o){
        return preg_replace('/\n?<.*?yoast.*?>/mi','',$o); }); });
    add_action('wp_head',function (){ ob_end_flush(); }, 999);
}

/**
 * Add SVG to WordPress
 */
function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

/**
 * Filters
 */
add_filter('the_content', 'filter_ptags_on_images'); // Stop p tag wrapping on images.
add_filter('admin_footer_text', 'change_footer_admin'); // Change built with wordpress
add_filter('login_headertitle', 'put_my_title'); // Change login image hover title
add_filter('login_headerurl', 'put_my_url'); // Change url for admin login
add_filter('gettext', 'back_to_site_text'); // Change back to site text
add_filter( 'show_recent_comments_widget_style', function() { return false; }); // Remove recent comment widget inline styles
add_filter( 'wpseo_json_ld_output', '__return_false' ); // Hides JSON output on front-end when SEO Yoastis activated.

/**
 * Add actions
 */
add_action( 'wp_enqueue_scripts', 'add_styles' ); // Add Theme Stylesheet
add_action( 'wp_enqueue_scripts', 'addJs' ); //enqueue scripts
add_action( 'init', 'menus' ); // Register menus
add_action( 'admin_init', 'df_disable_comments_admin_menu_redirect'); // Redirect any user trying to access comments page
add_action( 'login_enqueue_scripts', 'my_login_logo' ); // Change login page
add_action( 'wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0); // Remove WP logo from admin header
add_action( 'admin_init', 'remove_dashboard_meta' );
add_action( 'widgets_init', 'my_register_sidebars' ); // Register sidebar
add_action( 'after_setup_theme', 'image_sizes_thumbs', 11 ); //add image sizes

/**
 * Remove actions not needed
 */
remove_action( 'wp_print_styles', 'print_emoji_styles' ); // remove emoji styles
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // remove emoji styles
remove_action( 'wp_head', 'rsd_link' ); // EditURI link
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Category feed links
remove_action( 'wp_head', 'feed_links', 2 ); // Post and comment feed links
remove_action( 'wp_head', 'wlwmanifest_link' ); // Windows Live Writer
remove_action( 'wp_head', 'index_rel_link' ); // Index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // Previous link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // Start link
remove_action( 'wp_head', 'rel_canonical', 10, 0 ); // Canonical
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 ); // Shortlink
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for adjacent posts
remove_action( 'wp_head', 'wp_generator' ); // WP version


/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Register Custom Post Type - vacancies
function vacancies() {

	$labels = array(
		'name'                  => _x( 'Vacancies', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Vacancy', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Vacancies', 'text_domain' ),
        'all_items'             => __( 'All Vacancies', 'text_domain' ),
        'add_new_item'          => __( 'Add New Vacancy', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Vacancy', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'vacancies', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'public'                => true,
		'has_archive'           => true,
		'capability_type'       => 'page',
        'menu_position'         => 4,
        'menu_icon'             => 'dashicons-format-aside',
        'has_archive'           => true,
        'publicly_queryable'    => true,
        'taxonomies'            => array('genre','location'),
	);
	register_post_type( 'vacancies', $args );

}
add_action( 'init', 'vacancies', 0 );

// Register Custom Post Type - casestudies
function casestudies() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Case Studies', 'text_domain' ),
        'all_items'             => __( 'All Case Studies', 'text_domain' ),
        'add_new_item'          => __( 'Add New Case Study', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Case Study', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'casestudies', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'public'                => true,
		'has_archive'           => true,
		'capability_type'       => 'page',
        'menu_position'         => 4,
        'menu_icon'             => 'dashicons-groups',
        'has_archive'           => true,
        'publicly_queryable'    => true,
        'taxonomies'            => array('genre','location'),
        'rewrite'               => array('slug' => 'case-studies')

	);
	register_post_type( 'casestudies', $args );

}
add_action( 'init', 'casestudies', 0 );

/*------------------------------------*\
	Custom Post Type Taxonomies
\*------------------------------------*/

// hierarchical custom taxonomy - in place of categories
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
function create_topics_hierarchical_taxonomy() { 
    // services (for casestudies post type)
    $labels = array(
        'name' => _x( 'Services', 'taxonomy general name' ),
        'singular_name' => _x( 'Service', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Services' ),
        'all_items' => __( 'All Services' ),
        'parent_item' => __( 'Parent Service' ),
        'parent_item_colon' => __( 'Parent Service:' ),
        'edit_item' => __( 'Edit Service' ), 
        'update_item' => __( 'Update Service' ),
        'add_new_item' => __( 'Add New Service' ),
        'new_item_name' => __( 'New Service Name' ),
        'menu_name' => __( 'Services' ),
    );    
    
    register_taxonomy('services',array('casestudies'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'services' ),
    ));
}

// non-hierarchical custom taxonomy - in place of tags
add_action( 'init', 'create_topics_nonhierarchical_taxonomy', 0 );
function create_topics_nonhierarchical_taxonomy() {
    // channel (for casestudies post type)
    // was previously called 'technologies', but in order
    // not to lose content, only the admin names have been changed
    // call 'technologies' in theme instead of 'channels'
    $labels = array(
        'name' => _x( 'Channels', 'taxonomy general name' ),
        'singular_name' => _x( 'Channel', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Channels' ),
        'popular_items' => __( 'Popular Channels' ),
        'all_items' => __( 'All Channels' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Channel' ), 
        'update_item' => __( 'Update Channel' ),
        'add_new_item' => __( 'Add New Channel' ),
        'new_item_name' => __( 'New Channel Name' ),
        'separate_items_with_commas' => __( 'Separate Channels with commas' ),
        'add_or_remove_items' => __( 'Add or remove Channels' ),
        'choose_from_most_used' => __( 'Choose from the most used Channels' ),
        'menu_name' => __( 'Channels' ),
    ); 
  
    register_taxonomy('Technologies','casestudies',array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'technology' ),
    ));
    
    // new
    $labels = array(
        'name' => _x( 'Markets', 'taxonomy general name' ),
        'singular_name' => _x( 'Market', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Markets' ),
        'popular_items' => __( 'Popular Markets' ),
        'all_items' => __( 'All Markets' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Market' ), 
        'update_item' => __( 'Update Market' ),
        'add_new_item' => __( 'Add New Market' ),
        'new_item_name' => __( 'New Market Name' ),
        'separate_items_with_commas' => __( 'Separate Markets with commas' ),
        'add_or_remove_items' => __( 'Add or remove Markets' ),
        'choose_from_most_used' => __( 'Choose from the most used Markets' ),
        'menu_name' => __( 'Markets' ),
    ); 
  
    register_taxonomy('Markets','casestudies',array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'markets' ),
    ));
}



/*------------------------------------*\
	ACF PRO - Add Options
\*------------------------------------*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

/*------------------------------------*\
	Misc.
\*------------------------------------*/

/* Changed excerpt length to 150 words*/
function my_excerpt_length($length) {
    return 10;
}
add_filter('excerpt_length', 'my_excerpt_length');


/*------------------------------------*\
  Increase file upload size
\*------------------------------------*/

// @ini_set( 'upload_max_size' , '64M' );
// @ini_set( 'post_max_size', '64M');
// @ini_set( 'max_execution_time', '300' );