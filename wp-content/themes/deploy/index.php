<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Deploy
*
*/

get_header(); ?>

<div class="page blog">

	<div class="bg grey-bg slidebg" data-aos="slide-down" data-aos-delay="200" data-aos-duration="1000"></div>
	<section class="blog__archive animate">
		<div class="blog__archive--intro content-container padding addheight">
			<div class="content-wrapper">
				<?php
					$title = get_field('blog_archive_title', 54);
					$intro = get_field('blog_archive_intro', 54);
				?>
				<?= ( $title )? '<h1 class="title blog__archive--title" data-aos="fade-up">' . $title .'</h1>' : '';?>
				<?= ( $intro )? '<div class="blog__archive--content content" data-aos="fade-up" data-aos-delay="100">' . $intro .'</div>' : '';?>
			</div>
		</div> 
		
		<div class="blog__archive--built">
        	<?php get_template_part( 'partials/loop', 'blog' ); ?>
		</div>
		
		<div class="pagination addheight">
			<?php get_template_part( 'partials/content', 'pagination' ); ?>
		</div>
	</section>
	
	<?php
		$heading_quote = get_field('blog_quote_heading', 54);
		$text_quote = get_field('blog_quote_text', 54);
		$author_quote = get_field('blog_quote_author', 54);
	?>
	<section class="blog__quote quote section addheight">
		<div class="quote-content-container">
			<div class="content">
				<div class="content-wrapper">
					<?= ( $text_quote )? '<p class="text" data-aos="fade-in" data-aos-delay="100">' . $text_quote .'</p>' : '';?>
					<div class="author-wrapper">
						<?= ( $author_quote )? '<p class="author" data-aos="fade-in" data-aos-delay="150">' . $author_quote .'</p>' : '';?>
					</div>
				</div>
			</div>				
		</div>
	</section>
	
	<section class="blog__contact contact-partial section">
		<?php get_template_part( 'partials/partial', 'contact' ); ?>
	</section>
	
	<section class="blog__relationships section relationships">
		<?php get_template_part( 'partials/partial', 'relationships' ); ?>
	</section>
</div>

<?php get_footer(); ?>


