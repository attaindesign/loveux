<?php
/**
* The template for the sidebar containing the main widget area
*
* @package WordPress
* @subpackage Deploy
* 
*/
?>

    <?php if ( is_active_sidebar( 'primary' ) ) : ?>
    <aside class="sidebar" role="complementary">
    	<?php dynamic_sidebar( 'primary' ); ?>
    </aside>
    <?php endif; ?>