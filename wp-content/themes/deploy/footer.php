<?php
/**
* The template for displaying the footer
*
* Contains the closing of the "site-main" element and all content after
*
* @package WordPress
* @subpackage Deploy
*
*/
?>
                    <footer class="footer" role="contentinfo">
                        <?php get_template_part( 'partials/content', 'footer' ); ?>
                    </footer>                
                </div>
    		</main><!-- .site-main -->
    		<?php wp_footer(); ?>
        </div>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	</body>
</html>
