# Capistrano

> Use this tool to deploy repo to UAT and Live.

## Prerequisites (ruby is required)
  1. gem install capistrano
  1. gem install slackistrano
  1. gem install capistrano-rsync-bladrak
                

## Create user on WHM
  1. Create memorable username & password - store in 1Password
  1. Allow shell access

## cPanel Actions
Adding ssh keys and Databases to allow site to run.

1. Security -> SSH Access
1. Manage SSH Keys -> Import key
1. Add name for your key && paste your key into public key ( these will copy or Show your ssh key:
    
        (pbcopy < ~/.ssh/id_rsa.pub || cat ~/.ssh/id_rsa.pub) )


#### Domains -> Subdomains
##### UAT
1. Create new subdomain -> uat
1. Document root -> uat/current

##### Live
1. Document root -> production/current

#### Databases -> MySQL Databases
1. Create new database -> user_RandomLetters-live
1. Create new database -> user_RandomLetters-UAT
1. Create new user & password -> user_RandomLetters-live
1. Create new user & password -> user_RandomLetters-UAT

> *Assign Correct user to correct database.*

#### Databases -> phpMyAdmin -> 
Upload correct database to 
> user_RandomLetters-live

Upload correct database to 
> user_RandomLetters-uat

## SSH
* Documentroot: `mkdir production/`
* `cp -r ~/uat/shared ./` (edit contents specific to live)
* `rm -rf public_html/`
* `ln -s production/current public_html`
  
#### Override symlink for public_html to go to custom path
* `Creation -> ln -s {/path/to/file-name} {link-name}`
* `Update -> ln -sfn {/path/to/file-name} {link-name}`
* `e.g ln -s production/current public_html`

#### Add Cap tools to project
    cd ~/Sites/project/httpdocs/

#### Modify Cap files
Modify the cap files to suit your project - All vars are inside of `deploy.rb`

* Editing `deploy.rb` -> :application : change to name of theme
* Edit `deploy.rb` -> :repo_url : change to correct bitbucket url for this project.
* Edit `deploy.rb` -> :ssh_usr & :ssh_ip : change to correct settings for server.

## Deploying
  - SSH to remote

        ssh user@domain
        cd uat
        mkdir shared
        cd shared
        touch .htaccess wp-config.php
        nano .htaccess ( paste in correct htaccess contents )
        nano wp-config.php ( paste in correct wp-config.php )


  - Localhost

        cd ~/Sites/project/httpdocs/tools
        cap staging deploy || cap production deploy
        
>  Remember 
        
        mkdir -p wp-content/uploads && chmod 775 wp-content/uploads
        
>  Good tip: 
        
        go to /wp-admin and add better search and replace plugin to update old urls. Don't forget to delete plugin once finished with.
        
## Authors
* **Attain Design** - [AttainDesign](http://www.attaindesign.co.uk)
