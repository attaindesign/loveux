# Production server
server "#{fetch(:ssh_ip)}", user: "#{fetch(:ssh_usr)}", roles: %w{web app}
set :branch, 'master'