# Staging server
server "#{fetch(:ssh_ip)}", user: "#{fetch(:ssh_usr)}", roles: %w{web app}
if ENV['branch'] then
  set :branch, ENV['branch'] if ENV['branch']
else
  set :branch, 'master'
end

set :deploy_to, "/home/#{fetch(:ssh_usr)}/capistrano_uat"