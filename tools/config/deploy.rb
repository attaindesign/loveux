# config valid only for current version of Capistrano
lock "3.8.1"

set :application, "loveux"
set :repo_url, "git@bitbucket.org:attaindesign/loveux.git"

set :ssh_usr, "loveux.co.uk"
set :ssh_ip, "217.199.187.198"

set :ssh, "#{fetch(:ssh_usr)}@#{fetch(:ssh_ip)}"

set :scss, "wp-content/themes/#{fetch(:application)}/library/scss"
set :css, "wp-content/themes/#{fetch(:application)}/library/css"

set :scm, :rsync

set :rsync_options, %w[
    --recursive --delete --delete-excluded
    --exclude .git*
    --exclude .gitignore
    --exclude node_modules*
    --exclude /.htaccess
    --exclude wp-config.php
    --exclude gulpfile.js
    --exclude wp-content/themes/deploy/library/scss*
]

set :slackistrano, {
    klass: Slackistrano::CustomMessaging,
    channel: '#deploy',
    webhook: 'https://hooks.slack.com/services/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'
}

set :branch, proc { `git rev-parse --abbrev-ref develop`.chomp }

set :deploy_to, "/home/#{fetch(:ssh_usr)}/capistrano_production"
set :tmp_dir, "/home/#{fetch(:ssh_usr)}/cap/tmp"

set :deploy_via, :copy
set :tmp_dir, "/home/#{fetch(:ssh_usr)}/cap/tmp"
set :rsync_stage, "../../build"

set :linked_dirs, %w{wp-content/uploads}
set :linked_files, %w{.htaccess wp-config.php attain-rar-digital-awards-winner.png attain-rar-recommended.png}

set :keep_releases, 3

namespace :rsync do
    # Create an empty task to hook with. Implementation will be come next
    task :stage_done

    # On local machine before rsync
    after :stage_done, 'prelaunch:composer_install'
    after :stage_done, 'prelaunch:gulp_build'
end

namespace :deploy do
  # On live server
  # after 'deploy:updated', 'postlaunch:magento_remote_config'
end