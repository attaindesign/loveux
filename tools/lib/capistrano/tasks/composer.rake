namespace :prelaunch do
    desc "Composer install on local machine before rsync"
    task :composer_install do
        on roles(:app) do
            run_locally do
                execute "cd #{fetch(:rsync_stage)} && composer install"
            end
        end
    end
end