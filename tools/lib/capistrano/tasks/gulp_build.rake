namespace :prelaunch do
    desc "Config Wordpress on local machine before rsync"
    task :gulp_build do
        on roles(:app) do
            run_locally do
                execute "cd #{fetch(:rsync_stage)} && npm install && npm rebuild node-sass && gulp live"
            end
        end
    end
end
